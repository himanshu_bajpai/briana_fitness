1.For running the project first you have to chack whether node is installed or not on your system, if node is not installed you have to install the node.js first
2.If node is installed, you have to run the following command which will create a package.json file
npm init
3.You have to install a list of node modules for the proper functioning of the app, here is a list as given below:-
express, socket.io, body-parser, fcm-node, async, mysql, country-state-city, password-hash, @sendgrid/mail, randomstring
4.After installing the above modules, navigate to the directory where index.js is present and run the following command
node index