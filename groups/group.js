var con = require('../connection/connection');
var FCM = require('fcm-node');
var serverKey = 'AAAADJ64yO4:APA91bEd6FHBDYaORNh4FeUh81PSa8U_oQBTAaxV8A8r8Kv2v-HXaYcqOc8R_r2kAVgv1QkM5ZsPzhwj9rhohOgf3CeHbCed_TL9FzwwxM6b5ZeZdSpgXiHR25xbY2cgrX_DhnfLD9Na';
var fcm = new FCM(serverKey);
var async = require('async');

module.exports.create_group = function(req,res){
	var current_date = Math.floor(new Date().valueOf() / 1000);
	var members_length = parseInt(req.body.members_id.split(',').length) + 1;
	con.query("INSERT INTO groups (creator_id, name, image, created_at) VALUES ('" + req.body.creator_id + "', '" + req.body.name + "', '" + req.body.image + "', '" + current_date + "')",function(err,result){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while creating group"
	        })
		}else{
			var clients_id = req.body.members_id.split(',');
			con.query("SELECT name from trainers where id = '" + req.body.creator_id + "' ",function(err,trainer_data){
				if(err){
					res.send({
			            "status":0,
			            "message":"There is an error while fetching trainer data"
			        })
				}else{
					for(var i=0; i<clients_id.length; i++){
						con.query("SELECT device_token from users where id = '" + clients_id[i] + "' and notification = 'yes' ",function(err,token_data){
							if(err){
								res.send({
						            "status":0,
						            "message":"There is an error while fetching device token"
						        })
							}else{
								if(token_data != ''){
									var message = {
						    			to: token_data[0].device_token,
									    notification: {
									        title: 'Group join request', 
									        body: trainer_data[0].name + ' wants to add you in the group ' + req.body.name
									    },
									    data:{
									    	notification_type : "group_created",
									    	group_id : result.insertId
									    }
									};
									fcm.send(message,function(err,response){
									    if(err)
									       	console.log('Error')
									    else
									    	console.log('No Error')     
									})
								}
							}
						})

						con.query("INSERT into group_requests (group_id, trainer_id, client_id, status, created_at) values ('" + result.insertId + "', '" + req.body.creator_id + "', '" + clients_id[i] + "', 'Requested', '" + current_date + "') ",function(err,result2){
							if(err){
								res.send({
						            "status":0,
						            "message":"There is an error while fetching device token"
						        })
							}
						})
					}
					res.send({
		                "status":1,
		                "message":"Group created successfully"
		            })
				}
			})
		}
	})
}

module.exports.get_groups = function(req,res){
	if(req.body.type == 'trainer')
		var query = 'SELECT * from groups where creator_id = "' + req.body.id + '" ';
	else
		var query = 'SELECT groups.*,group_members.is_removed from groups inner join group_members ON groups.id = group_members.group_id where group_members.member_id = "' + req.body.id + '" ';

	con.query(query,function(err,group_data){
		if(err){
			res.send({
                "status":0,
                "message":"There is an error while fetching group data"
            })
		}else{
			if(!group_data.length){
				res.send({
		            "status":1,
		            "message":"No group exist",
		            "data":group_data
		        })
			}else{
				var final_data = [];
            	var i = 0;
            	async.mapSeries(group_data, function(data,callback){
            		con.query("SELECT count(id) as members from group_members where group_id = '" + data.id + "' and is_removed = '0' ",function(err,rows){
            			if(err){
            				res.send({
				                "status":0,
				                "message":"There is an error while fetching group members data"
				            })
            			}else{
            				data.members = parseInt(rows[0].members) + 1;
            				final_data[i] = data;
				    		i++;
			        		return callback(null, final_data);
            			}
            		})
            	},function(err,results){
	                res.send({
			            "status":1,
			            "message":"Group exist",
			            "data":final_data
			        })
	            });
			}
		}
	})
}

module.exports.get_group_members = function(req,res){
	con.query("SELECT name,image,creator_id from groups where id = '" + req.body.group_id + "' ",function(err,group_data){
		if(err){
			res.send({
                "status":0,
                "message":"There is an error while fetching group data"
            })
		}else{
			con.query("SELECT GROUP_CONCAT(member_id) as member_id from group_members where group_id = '" + req.body.group_id + "' and is_removed = '0' ",function(err,rows_new){
				if(err){
					res.send({
		                "status":0,
		                "message":"There is an error while fetching group members data"
		            })
				}else{
					if(rows_new[0].member_id != '')
						var query = "SELECT id as user_id,name,profile_image from users where id IN (" + rows_new[0].member_id + ") ";
					else
						var query = "SELECT id as user_id,name,profile_image from users where id = '" + rows_new[0].member_id + "' ";

					con.query(query,function(err, rows){
		        		if(err){
		        			res.send({
				                "status":0,
				                "message":"There is an error while fetching members data"
				            })
		        		}else{
		        			for(var i=0; i< rows.length; i++){
		        				rows[i].admin = 'false';
		        			}
		        			con.query("SELECT id as user_id,name,profile_image from trainers where id = '" + group_data[0].creator_id + "' ",function(err, rows2){
		        				if(err){
				        			res.send({
						                "status":0,
						                "message":"There is an error while fetching trainer data"
						            })
				        		}else{
				        			rows2[0].admin = 'true';
				        			rows.push(rows2[0]);
				        			con.query("SELECT GROUP_CONCAT(member_id) as member_id from group_members where group_id = '" + req.body.group_id + "' and is_removed = '1' ",function(err,rows3){
										if(err)
											throw err;

										if(rows3[0].member_id == null)
											rows3[0].member_id = '';

										con.query("SELECT users.name, users.profile_image from users inner join group_requests on users.id = group_requests.client_id where group_requests.group_id = '" + req.body.group_id + "' ",function(err,rows4){
											if(err){
												res.send({
									                "status":0,
									                "message":"There is an error while fetching requested members data"
									            })
											}else{
												res.send({
								                    "status":1,
								                    "message":"Successful",
								                    "group_name": group_data[0].name,
								                    "group_image": group_data[0].image,
								                    "deleted_member_ids": rows3[0].member_id,
								                    "data" : rows,
								                    "requested_members_data" : rows4
								                });
											}
										})
									})
				        		}
		        			})
		        		}
		        	})
				}
			})
		}
	})
}

module.exports.send_post = function(req,res){
	var current_date = Math.floor(new Date().valueOf() / 1000);
	con.query("INSERT INTO group_posts(group_id, user_id, post_text, post_title, post_type, post_url, type, thumbnail, created_at) VALUES ('" + req.body.group_id + "', '" + req.body.user_id + "', '" + req.body.post_text + "', '" + req.body.post_title + "', '" + req.body.post_type + "', '" + req.body.post_url + "', '" + req.body.type + "', '" + req.body.thumbnail + "', '" + current_date + "')",function(err,result){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while sending post in the group"
	        })
		}else{
			res.send({
                "status":1,
                "message":req.body.post_type + ' has been posted successfully in the group'
            });

            con.query("SELECT count(member_id) as count from group_members where group_id = '" + req.body.group_id + "' and is_removed = 0 ",function(err,rows){
            	if(err)
            		console.log(err)
            	else{
            		if(rows[0].count > 1){
            			if(req.body.type == 'client'){
            				//SEND NOTIFICATION TO ALL THE CLIENTS OF THE GROUP (EXCLUDING CURRENT USER), INCLUDING TRAINER
            				var query = "SELECT groups.name,groups.creator_id,groups.is_deleted,groups.image,users.device_token from groups inner join group_members on groups.id = group_members.group_id inner join users on group_members.member_id = users.id where groups.id = '" + req.body.group_id + "' and group_members.is_removed = 0 and group_members.member_id != '" + req.body.user_id + "' and users.notification = 'yes' ";

            				var sender_name_query = "SELECT name from users where id = '" + req.body.user_id + "' "
            			}else{
            				//SEND NOTIFICATION TO ALL THE CLIENTS OF THE GROUP EXCLUDING TRAINER
            				var query = "SELECT groups.name,groups.creator_id,groups.is_deleted,groups.image,users.device_token from groups inner join group_members on groups.id = group_members.group_id inner join users on group_members.member_id = users.id where groups.id = '" + req.body.group_id + "' and group_members.is_removed = 0 and users.notification = 'yes' ";

            				var sender_name_query = "SELECT name from trainers where id = '" + req.body.user_id + "' "
            			}
            		}else{
            			if(req.body.type == 'client'){
            				//SEND NOTIFICATION TO TRAINER ONLY
            				var query = "SELECT groups.name,groups.creator_id,groups.image,groups.is_deleted,trainers.device_token from groups inner join trainers on groups.creator_id = trainers.id where groups.id = '" + req.body.group_id + "' and trainers.notification = 'yes' ";

            				var sender_name_query = "SELECT name from users where id = '" + req.body.user_id + "' "
            			}else{
            				//SEND NOTIFICATION TO CLIENT ONLY
            				var query = "SELECT groups.name,groups.creator_id,groups.is_deleted,groups.image,users.device_token from groups inner join group_members on groups.id = group_members.group_id inner join users on group_members.member_id = users.id where groups.id = '" + req.body.group_id + "' and group_members.is_removed = 0 and users.notification = 'yes' ";

            				var sender_name_query = "SELECT name from trainers where id = '" + req.body.user_id + "' "
            			}
            		}
            		if(query != undefined){
            			con.query(query,function(err,rows2){
	            			if(err)
	            				console.log(err)
	            			else{
	            				con.query("SELECT count(id) as count from group_members where group_id = '" + req.body.group_id + "' and is_removed = 0",function(err,count_rows){
	            					if(err)
	            						console.log(err)
	            					else{
	            						con.query(sender_name_query,function(err,sender_name_rows){
	            							if(err)
	            								console.log(err)
	            							else{
	            								for(var i=0; i< rows2.length; i++){
	            									if(rows2 != ''){
	            										var message = {
									    					to: rows2[i].device_token,
														    notification: {
														        title: 'New Post', 
														        body: sender_name_rows[0].name + ' sends a post in the ' + rows2[i].name
														    },
														    data:{
														    	"creator_id" : rows2[i].creator_id,
														    	"id" : req.body.group_id,
														    	"members" : parseInt(count_rows[0].count) + 1,
														    	"name" : rows2[i].name,
														    	"image" : rows2[i].image,
														    	"notification_type" : 'new_post',
														    	"is_deleted" : rows2[i].is_deleted,
														    	"is_removed" : 0,
														    }
														};

														fcm.send(message,function(err,response){
														    if(err)
														       	console.log('Error')
														    else
														    	console.log('No Error')     
														})
	            									}
					            				}
					            				if(rows[0].count > 1){
					            					if(req.body.type == 'client'){
					            						con.query("SELECT trainers.device_token from groups inner join trainers on groups.creator_id = trainers.id where groups.id = '" + req.body.group_id + "' and trainers.notification = 'yes' ",function(err,rows3){
					            							if(err)
					            								console.log(err)
					            							else{
					            								if(rows3 != ''){
					            									var message = {
												    					to: rows3[0].device_token,
																	    notification: {
																	        title: 'New Post', 
																	        body: sender_name_rows[0].name + ' sends a post in the ' + rows2[0].name
																	    },
																	    data:{
																	    	"creator_id" : rows2[0].creator_id,
																	    	"id" : req.body.group_id,
																	    	"members" : parseInt(count_rows[0].count) + 1,
																	    	"name" : rows2[0].name,
																	    	"image" : rows2[0].image,
																	    	"notification_type" : 'new_post',
																	    	"is_deleted" : rows2[0].is_deleted,
																	    	"is_removed" : 0,
																	    }
																	};
																	fcm.send(message,function(err,response){
																	    if(err)
																	       	console.log('Error')
																	    else
																	    	console.log('No Error')     
																	})
					            								}
					            							}
					            						})
					            					}
					            				}
	            							}
	            						})
	            					}
	            				})
            				}
            			})
            		}
            	}
            })
		}
	});
}

module.exports.get_posts_list = function(req,res){
	var limit = 15;
    var page_number = req.body.page_number;  
    var start_from = (page_number-1) * limit;

	con.query("SELECT * from group_posts where group_id = '" + req.body.group_id + "' ORDER BY ID DESC LIMIT " + start_from + "," + limit + " ",function(err,post_data){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while fetching posts from group",
	            "data_exist": "false"
	        })
		}else{
			if(!post_data.length){
				res.send({
		            "status":1,
		            "message":"No post exist for this group",
		            "data_exist": "false",
		            "data":post_data
		        })
			}else{
				if(post_data.length < limit)
                	var data_exist = 'false';
            	else
                	var data_exist = 'true';
                
                var final_data = [];
            	var i = 0;
				async.mapSeries(post_data, function(data,callback){
					if(data.type == 'client')
						var query2 = "SELECT name,profile_image from users where id = '" + data.user_id + "' ";
					else
						var query2 = "SELECT name,profile_image from trainers where id = '" + data.user_id + "' ";

					con.query(query2,function(err, user_data){
						if(err){
	                        res.send({
	                            "status":0,
	                            "message":"error ocurred while fetching comments data"
	                        })
	                    }
	                    con.query("SELECT count(post_id) as comments_count from post_comments where post_id IN (" + data.id + ") ", function(err,rows2){
		                    if(err){
		                        res.send({
		                            "status":0,
		                            "message":"error ocurred while fetching comments data"
		                        })
		                    }
		                    con.query("SELECT count(post_id) as likes_count from post_likes where post_id IN (" + data.id + ") ", function(err,rows3){
			                    if(err){
			                        res.send({
			                            "status":0,
			                            "message":"error ocurred while fetching likes count data"
			                        })
			                    }
			                    con.query("SELECT post_id as likes_post_id from post_likes where post_id IN (" + data.id + ") and user_id = '" + req.body.user_id + "' and type = '" + req.body.type + "' ", function(err,rows4){
			                    	if(err){
				                        res.send({
				                            "status":0,
				                            "message":"error ocurred while fetching likes data"
				                        })
				                    }
				                    if(rows4 != '')
				                		data.like_status = 'true';
				              		else
				                		data.like_status = 'false';

				                	if(user_data != ''){
				                		data.user_name = user_data[0].name
				                		data.profile_image = user_data[0].profile_image
				                	}
				                    data.comments_count = rows2[0].comments_count;
				                    data.likes_count = rows3[0].likes_count;
				                    final_data[i] = data;
				                    i++;

			                    	return callback(null, final_data);
			                    })
			                })
	                	});
					})
	            },function(err,results){
	                res.send({
			            "status":1,
			            "message":"Post exist for this group",
			            "data_exist": data_exist,
			            "data":final_data
			        })
	            });
			}
		}
	})
}

module.exports.delete_rooms_from_list = function(req,res){
	var deleted_at = Math.floor(new Date().valueOf() / 1000);
	con.query("SELECT id from delete_rooms where user_id = '" + req.body.user_id + "' and user_type = '" + req.body.user_type + "' and chat_id = '" + req.body.chat_id + "' and chat_type = '" + req.body.chat_type + "' ",function(err,rows){
		if(err){
			res.send({
				"status" :0,
				"message": "Error while deleting room from list"
			})
		}else{
			if(!rows.length){
				con.query("INSERT into delete_rooms (user_id,user_type,chat_id,chat_type,deleted_at) values('" + req.body.user_id + "','" + req.body.user_type + "', '" + req.body.chat_id + "', '" + req.body.chat_type + "', '" + deleted_at + "') ",function(err,result){
					if(err){
						res.send({
							"status" : 0,
							"message": "Error while deleting room from list"
						})
					}else{
						res.send({
							"status" : 1,
							"message": "Room deleted successfully"
						})
					}
				})
			}else{
				con.query("UPDATE delete_rooms SET deleted_at = '" + deleted_at + "' where user_id = '" + req.body.user_id + "' and user_type = '" + req.body.user_type + "' and chat_id = '" + req.body.chat_id + "' and chat_type = '" + req.body.chat_type + "' ",function(err,result){
					if(err){
						res.send({
							"status" : 0,
							"message": "Error while deleting room from list"
						})
					}else{
						res.send({
							"status" : 1,
							"message" : "Room deleted successfully"
						})
					}
				})
			}
		}
	})
}

