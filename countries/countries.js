var countries_module = require('country-state-city')

module.exports.get_countries = function(req,res){
	var countries_data = countries_module.getAllCountries()
	if(countries_data.length){
		res.send({
			'message' : 'Countries exist',
			'data' : countries_data
		})
	}else{
		res.send({
			'message' : 'There is an error while fetching countries',
			'data' : []
		})
	}
}

module.exports.get_states = function(req,res){
	var states_data = countries_module.getStatesOfCountry(req.body.country_id)
	if(states_data.length){
		res.send({
			'message' : 'States exist',
			'data' : states_data
		})
	}else{
		res.send({
			'message' : 'There is an error while fetching states',
			'data' : []
		})
	}
}