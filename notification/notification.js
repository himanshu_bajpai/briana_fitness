var con = require('../connection/connection');

module.exports.get_notification_status = function(req,res){
	if(req.body.type == 'client')
		var query = "SELECT notification from users where id = '" + req.body.user_id + "' ";
	else
		var query = "SELECT notification from trainers where id = '" + req.body.user_id + "' ";

	con.query(query,function(err,rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while fetching notification status data"
	        })
		}else{
			res.send({
	            "status":1,
	            "message":"Notification status",
	            "data":rows[0].notification
	        })
		}
	})
}

module.exports.update_notification_status = function(req,res){
	if(req.body.type == 'client')
		var query = "UPDATE users SET notification = '" + req.body.notification_status + "' where id = '" + req.body.user_id + "' "
	else
		var query = "UPDATE trainers SET notification = '" + req.body.notification_status + "' where id = '" + req.body.user_id + "' "

	con.query(query,function(err,rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while updating notification status data"
	        })
		}else{
			res.send({
	            "status":1,
	            "message":"Notification status updated"
	        })
		}
	})
}
