var express = require('express')
var app   = express()
var http = require('http').Server(app)
var io = require('socket.io').listen(http)
var bodyParser = require("body-parser")
var jsonParser = bodyParser.json()
var router = express.Router()
var register = require('./users/register')
var login = require('./users/login')
var profile = require('./users/profile')
var password = require('./users/password')
var countries = require('./countries/countries')
var logout = require('./users/logout')
var trainers = require('./trainers/trainers')
var clients = require('./clients/clients')
var groups = require('./groups/group')
var workout = require('./workout/workout')
var FCM = require('fcm-node');
var serverKey = 'AAAADJ64yO4:APA91bEd6FHBDYaORNh4FeUh81PSa8U_oQBTAaxV8A8r8Kv2v-HXaYcqOc8R_r2kAVgv1QkM5ZsPzhwj9rhohOgf3CeHbCed_TL9FzwwxM6b5ZeZdSpgXiHR25xbY2cgrX_DhnfLD9Na';
var fcm = new FCM(serverKey);
var async = require('async');
var notification = require('./notification/notification')
var urlencodedParser = bodyParser.urlencoded({ extended: true });

app.use(bodyParser());

app.get('/',function(req,res,next){
	res.sendFile(__dirname + '/index.html')
});

app.get('/forgot_password',function(req,res,next){
	res.sendFile(__dirname + '/forgot_password.html')
});

app.post('/submit_form', urlencodedParser, function (req, res){
	if(req.body.password != req.body.confirm_password){
		var msg = "Password does not match";
		res.sendFile(__dirname + '/forgot_password.html', {'msg' : msg})
	}else{
		var msg = "Password match";
		res.sendFile(__dirname + '/forgot_password.html', {'msg' : msg})
	}
 });

app.use(bodyParser.urlencoded({ extended: true }))
app.use('/api', router)

router.post('/client_register', register.client_register)
router.post('/trainer_register', register.trainer_register)

router.post('/login', login.login)
router.post('/social_login', login.social_login)

router.post('/get_profile', profile.get_profile)
router.post('/client_edit_profile', profile.client_edit_profile)
router.post('/trainer_edit_profile', profile.trainer_edit_profile)

router.post('/change_password', password.change_password)
router.post('/forgot_password', password.forgot_password)

router.get('/get_countries', countries.get_countries)
router.post('/get_states', countries.get_states)

router.post('/get_all_trainers', clients.get_all_trainers)
router.post('/request_trainer', clients.request_trainer)
router.post('/get_trainer_list', clients.get_trainer_list)
router.post('/get_requested_trainer_list', clients.get_requested_trainer_list)
router.post('/request_status_trainer', clients.request_status_trainer)
router.get('/get_fitness_levels', clients.get_fitness_levels)
router.post('/get_requested_group_requests', clients.get_requested_group_requests)

router.post('/request_status', trainers.request_status)
router.post('/get_client_list', trainers.get_client_list)
router.post('/get_requested_client_list', trainers.get_requested_client_list)
router.post('/delete_client', trainers.delete_client)
router.post('/get_all_clients', trainers.get_all_clients)
router.post('/request_client', trainers.request_client)
router.post('/get_client_profile', trainers.get_client_profile)
router.post('/get_group_request_status', trainers.get_group_request_status)

router.post('/create_group', groups.create_group)
router.post('/get_groups', groups.get_groups)
router.post('/get_group_members', groups.get_group_members)
router.post('/send_post', groups.send_post)
router.post('/get_posts_list', groups.get_posts_list)
router.post('/delete_rooms_from_list', groups.delete_rooms_from_list)

router.post('/get_notification_status', notification.get_notification_status)
router.post('/update_notification_status', notification.update_notification_status)

router.get('/get_workout_categories', workout.get_workout_categories)
router.post('/get_workout_based_on_category', workout.get_workout_based_on_category)
router.post('/get_workout_details', workout.get_workout_details)
router.get('/get_exercises', workout.get_exercises)
router.post('/create_workout', workout.create_workout)
router.post('/client_list_for_assigning_workout', workout.client_list_for_assigning_workout)
router.post('/assign_workout', workout.assign_workout)
router.post('/get_trainer_workout', workout.get_trainer_workout)
router.post('/get_trainer_workout_details', workout.get_trainer_workout_details)
router.post('/get_client_workout', workout.get_client_workout)
router.post('/get_today_workout', workout.get_today_workout)
router.post('/edit_workout', workout.edit_workout)

router.post('/logout', logout.logout)

http.listen(8001, function() {
   	console.log('app started listening on 8001')
});

var con = require('./connection/connection');
io.sockets.on('connection',function(socket){
	socket.on('add_clients',function(data){
		if(data.members_id == ''){
			//Not adding a client in the group, only update group
			con.query("UPDATE groups SET name = '" + data.group_name + "', image = '" + data.group_image + "' where id = '" +  data.group_id + "' ",function(err,result){
				if(err)
					throw err;

				con.query("SELECT GROUP_CONCAT(member_id) as member_id from group_members where group_id = '" + data.group_id + "' and is_removed = '0' ",function(err,rows){
					if(err)
						throw err;

					con.query("SELECT count(id) as count from group_members where group_id = '" + data.group_id + "' and is_removed = '0' ",function(err,rows2){
						if(err)
							throw err;

						var count = parseInt(rows2[0].count) + 1;
						var final_data = {
							'group_id' : data.group_id,
							'creator_id' : data.creator_id,
							'name' : data.group_name,
							'members_id' : rows[0].member_id,
							'members' : count
						}
						io.sockets.emit('clients_added',final_data);
					})
				})
			})
		}else{
			//Adding a client in the group
			var member_ids = data.members_id.split(',');
			async.mapSeries(member_ids,function(new_data,callback){
    			con.query("SELECT name from trainers where id = '" + data.creator_id + "' ",function(err,trainer_data){
					if(err)
						throw err;

					else{
						con.query("SELECT device_token from users where id = '" + new_data + "' and notification = 'yes' ",function(err,token_data){
							if(err)
								throw err;

							else{
								var message = {
					    			to: token_data[0].device_token,
								    notification: {
								        title: 'Group join request', 
								        body: trainer_data[0].name + ' wants to add you in the group ' + data.group_name
								    },
								    data:{
								    	notification_type : "group_created",
								    	group_id : data.group_id
								    }
								};
								fcm.send(message,function(err,response){
								    if(err)
								       	console.log(err);
								    else
								    	console.log('elseeeeeeee')     
								})
							}
						})
					}
				});
    			var current_date = Math.floor(new Date().valueOf() / 1000);
				con.query("INSERT into group_requests (group_id, trainer_id, client_id, status, created_at) values ('" + data.group_id + "', '" + data.creator_id + "', '" + new_data + "', 'Requested', '" + current_date + "') ",function(err,result2){
					if(err){
						res.send({
				            "status":0,
				            "message":"There is an error while fetching device token"
				        })
					}
				})
				return callback(null,data);	
			},function(err,results){
				con.query("SELECT GROUP_CONCAT(member_id) as member_id from group_members where group_id = '" + data.group_id + "' and is_removed = '0' ",function(err,rows){
					if(err)
						throw err;

					con.query("SELECT count(id) as count from group_members where group_id = '" + data.group_id + "' and is_removed = '0' ",function(err,rows2){
						if(err)
							throw err;

						var count = parseInt(rows2[0].count) + 1;
						var final_data = {
							'group_id' : data.group_id,
							'creator_id' : data.creator_id,
							'name' : data.group_name,
							'members_id' : rows[0].member_id + ',' + data.members_id,
							'members' : count
						}
						io.sockets.emit('clients_added',final_data);
					})
				})
    		});
		}
	});

	socket.on('remove_clients',function(data){
		con.query("UPDATE group_members SET is_removed = '1' where group_id = '" + data.group_id + "' and member_id = '" + data.user_id + "' ",function(err,result){
    		if(err)
    			throw err;

    		con.query("SELECT name, creator_id from groups where id = '" + data.group_id + "' ",function(err,group_data){
				if(err)
					throw err;

				con.query("SELECT GROUP_CONCAT(member_id) as member_id from group_members where group_id = '" + data.group_id + "' and is_removed = '0' ",function(err,rows){
					if(err)
						throw err;

					con.query("SELECT count(id) as count from group_members where group_id = '" + data.group_id + "' and is_removed = '0' ",function(err,rows2){
						if(err)
							throw err;

						var count = parseInt(rows2[0].count) + 1;
						var final_data = {
							'id' : data.group_id,
							'creator_id' : group_data[0].creator_id,
							'name' : group_data[0].name,
							'members_id' : rows[0].member_id,
							'user_id' : data.user_id,
							'members' : count
						}
		    			io.sockets.emit('client_removed',final_data);
					})
				})
			})
    	})
	})

	socket.on('comment',function(data){
		var current_date = Math.floor(new Date().valueOf() / 1000);
		con.query("INSERT INTO post_comments (post_id, user_id, comment, type, created_at) VALUES ('" + data.post_id + "', '" + data.user_id + "', '" + data.comment + "', '" + data.type + "', '" + current_date + "')",function(err,result){
			if(err)
				throw err;

			if(data.type == 'trainer')
				var query = "SELECT name, profile_image from trainers where id = '" + data.user_id + "' ";
			else
				var query = "SELECT name, profile_image from users where id = '" + data.user_id + "' ";

			con.query(query,function(err,user_data){
				if(err)
					throw err;

				con.query("SELECT count(post_id) as count from post_comments where post_id = '" + data.post_id + "' ", function(err, comment_count){
					if(err)
						throw err;

					var final_data = {
						'post_id' : data.post_id,
						'user_id' : data.user_id,
						'user_name' : user_data[0].name,
						'profile_image' : user_data[0].profile_image,
						'id' : result.insertId,
						'comment' : data.comment,
						'type' : data.type,
						'created_at' : current_date,
						'comments_count' : comment_count[0].count
					}
					io.sockets.emit('comment_posted',final_data);
				})
			})
		})
	})

	socket.on('like',function(data){
		var current_date = Math.floor(new Date().valueOf() / 1000);
		if(data.status == 'true'){
			con.query("INSERT INTO post_likes (post_id, user_id, type, created_at) VALUES ('" + data.post_id + "', '" + data.user_id + "', '" + data.type + "', '" + current_date + "')",function(err,result){
				if(err)
					throw err;

				con.query("SELECT count(post_id) as count from post_likes where post_id = '" + data.post_id + "' ", function(err,likes_data){
					if(err)
						throw err;

					var like_data = {
						'id' : data.post_id,
						'user_id' : data.user_id,
						'like_status' : data.status,
						'likes_count' : likes_data[0].count,
						'type' : data.type
					}
					io.sockets.emit('like_status',like_data);
				})
			})
		}else{
			con.query('DELETE FROM post_likes WHERE user_id = "' + data.user_id + '" and post_id = "' + data.post_id + '" ',function(err,result){
		    	if(err)
		      		throw err;

		      	con.query("SELECT count(post_id) as count from post_likes where post_id = '" + data.post_id + "' ", function(err,likes_data){
					if(err)
						throw err;

					var like_data = {
						'id' : data.post_id,
						'user_id' : data.user_id,
						'like_status' : data.status,
						'likes_count' : likes_data[0].count,
						'type' : data.type
					}
					io.sockets.emit('like_status',like_data);
				})
		    });
		}
	})

	socket.on('get_post_comments',function(data){
		var final_result = {}
		con.query("SELECT id, user_id, comment, type, created_at from post_comments where post_id = '" + data.post_id + "' ",function(err,post_comment_data){
			if(err)
				throw err;

			var final_data = [];
            var i = 0;
			async.mapSeries(post_comment_data,function(new_data,callback){
				if(new_data.type == 'client')
					var query2 = "SELECT name,profile_image from users where id = '" + new_data.user_id + "' ";
				else
					var query2 = "SELECT name,profile_image from trainers where id = '" + new_data.user_id + "' ";

				con.query(query2,function(err,user_data){
					if(err)
						throw err;

					if(user_data != ''){
                		new_data.user_name = user_data[0].name
                		new_data.profile_image = user_data[0].profile_image
                	}
                	final_data[i] = new_data;
                	final_result.data = final_data;
                	final_result.post_id = data.post_id;
                	final_result.user_id = data.user_id;
				    i++;

                	return callback(null,final_result);
				})
			},function(err,results){
                io.sockets.emit('post_comments_data',final_result);
            });
		})
	})

	socket.on('delete_comment',function(data){
		con.query("SELECT * from post_comments where id = '" + data.comment_id + "' ",function(err,comment_data){
			if(err)
				throw err;

			if(comment_data[0].type == 'client')
				var query = "SELECT name, profile_image from users where id = '" + comment_data[0].user_id + "' ";
			else
				var query = "SELECT name, profile_image from trainers where id = '" + comment_data[0].user_id + "' ";

			con.query(query,function(err,user_data){
				if(err)
					throw err;

				con.query("DELETE from post_comments where id = '" + data.comment_id + "' ",function(err,result){
					if(err)
						throw err;

					comment_data[0].user_name = user_data[0].name;
					comment_data[0].profile_image = user_data[0].profile_image;
					io.sockets.emit('response_delete_comment',comment_data);
				})
			})
		})
	})

	socket.on('delete_post',function(data){
		con.query("SELECT group_id, user_id from group_posts where id = '" + data.post_id + "' ",function(err,rows){
			if(err)
				throw err;

			con.query("DELETE from group_posts where id = '" + data.post_id + "' ",function(err,result){
				if(err)
					throw err;

				con.query("DELETE from post_comments where post_id = '" + data.post_id + "' ",function(err,result2){
					if(err)
						throw err;

					con.query("DELETE from post_likes where post_id = '" + data.post_id + "' ",function(err,result3){
						if(err)
							throw err;

						var response_data = {
							"group_id" : rows[0].group_id,
							"user_id" : rows[0].user_id,
							"id" : data.post_id
						}
						io.sockets.emit('post_deleted',response_data);
					})
				})
			})
		})
	})

	socket.on('group_left',function(data){
		con.query("UPDATE group_members SET is_removed = '1' where group_id = '" + data.group_id + "' and member_id = '" + data.user_id + "' ",function(err,result){
    		if(err)
    			throw err;

    		con.query("SELECT name, creator_id from groups where id = '" + data.group_id + "' ",function(err,group_data){
				if(err)
					throw err;

				con.query("SELECT GROUP_CONCAT(member_id) as member_id from group_members where group_id = '" + data.group_id + "' and is_removed = '0' ",function(err,rows){
					if(err)
						throw err;

					con.query("SELECT count(id) as count from group_members where group_id = '" + data.group_id + "' and is_removed = '0' ",function(err,rows2){
						if(err)
							throw err;

						var count = parseInt(rows2[0].count) + 1;
						var final_data = {
							'group_id' : data.group_id,
							'creator_id' : group_data[0].creator_id,
							'name' : group_data[0].name,
							'members_id' : rows[0].member_id,
							'user_id' : data.user_id,
							'members' : count
						}
		    			io.sockets.emit('group_left_by_you',final_data);
					})
				})
			})
    	})
	})

	socket.on('delete_group',function(data){
		con.query("UPDATE groups SET is_deleted = 'yes' where id = '" + data.group_id + "' ",function(err,result){
        	if(err)
        		throw err;

        	var response_data = {
				"id" : data.group_id
			}
			io.sockets.emit('group_deleted',response_data);
        });
	})

	socket.on('send_group_message',function(data){
		var current_date = Math.floor(new Date().valueOf() / 1000);
		con.query("INSERT INTO group_messages (group_id, sender_id, sender_type, message, message_type, video_thumbnail, created_at) VALUES ('" + data.group_id + "', '" + data.sender_id + "', '" + data.sender_type + "', '" + data.message + "', '" + data.message_type + "', '" + data.video_thumbnail + "', '" + current_date + "')", function (err,result){
			if(err)
				throw err;

			data.message_timestamp = current_date;
			data.message_id = result.insertId;
			io.sockets.emit('group_message_sent', data)

			//Send message notification
			con.query("SELECT users.device_token,users.id from group_members INNER JOIN users ON group_members.member_id = users.id where group_members.group_id = '" + data.group_id + "' and group_members.is_removed = 0 and users.notification = 'yes' ",function(err, rows){
				if(err)
					throw err;

				if(data.message_type == 'text')
					var body = data.message;
				else if(data.message_type == 'image')
					var body = 'sent an image';
				else
					var body = 'sent a video';

				for(var i=0;i<rows.length;i++){
					if(rows[i].id != data.sender_id){
						var message = {
							to: rows[i].device_token,
						    notification: {
						        title: data.sender_name + ' @ ' + data.group_name, 
						        body: body
						    },
						    data: {
			                    "sender_name" : data.sender_name,
								"group_name" : data.group_name,
								"group_id" : data.group_id,
								"chat_type" : 'group',
								"type" : data.sender_type,
			                    "notification_type" : 'Group chat'
			                }
						};
						fcm.send(message,function(err,response){
						    if(err)
						       	console.log('errrrrrrrrr')
						    else
						    	console.log('elseeeeeeee')     
						})
					}
				}
				if(data.sender_type != 'trainer'){
					con.query("SELECT trainers.device_token from groups INNER JOIN trainers ON groups.creator_id = trainers.id where groups.id = '" + data.group_id + "' and trainers.notification = 'yes' ",function(err,rowss){
						if(err)
							throw err;

						if(rowss != ''){
							var message2 = {
								to: rowss[0].device_token,
							    notification: {
							        title: data.sender_name + ' @ ' + data.group_name, 
							        body: body
							    },
							    data: {
				                    "sender_name" : data.sender_name,
									"group_name" : data.group_name,
									"group_id" : data.group_id,
									"chat_type" : 'group',
									"type" : data.sender_type,
				                    "notification_type" : 'Group chat'
				                }
							};
							fcm.send(message2,function(err,response){
							    if(err)
							       	console.log(err)
							    else
							    	console.log('elseeeeeeee')     
							})
						}
					})
				}
			})
		})
	})

	socket.on("create_room",function(data){
		var current_date = Math.floor(new Date().valueOf() / 1000);
		var room_name = data.trainer_id + '_' + data.client_id;
		con.query("SELECT room_id from rooms where room_name = '" + room_name + "' ",function(err,rows){
			if(err)
				throw err;

			if(rows.length){
				var response_data = {
					"room_id" : rows[0].room_id
				}
				io.sockets.emit('room_created',response_data);
			}else{
				con.query("INSERT INTO rooms(room_name, created_at) VALUES ('" + room_name + "', '" + current_date + "')", function(err,result){
					if(err)
						throw err;

					var response_data = {
						"room_id" : result.insertId
					} 

					io.sockets.emit('room_created',response_data);
				})
			}
		})
	})

	socket.on('send_private_message',function(data){

		var current_date = Math.floor(new Date().valueOf() / 1000);
		con.query("INSERT INTO private_messages (room_id, sender_id, sender_type, receiver_id, message, message_type, video_thumbnail, created_at) VALUES ('" + data.room_id + "','" + data.sender_id + "', '" + data.sender_type + "', '" + data.receiver_id + "', '" + data.message + "', '" + data.message_type + "', '" + data.video_thumbnail + "', '" + current_date + "')", function (err,result2){
			if(err)
				throw err;

			data.message_timestamp = current_date;
			data.message_id = result2.insertId;
			io.sockets.emit('private_message_sent',data);

			if(data.sender_type == 'trainer')
				var query = "SELECT name,device_token from users where id = '" + data.receiver_id + "' and notification = 'yes' ";
			else
				var query = "SELECT name,device_token from trainers where id = '" + data.receiver_id + "' and notification = 'yes' ";

			con.query(query,function(err,rowss){
				if(err)
					throw err;

				if(rowss != ''){
					if(data.message_type == 'text'){
						var title = data.sender_name + ' sents a message';
						var body = data.message 
					}else if(data.message_type == 'image'){
						var title = data.sender_name + ' sents an image';
						var body = '';
					}else{
						var title = data.sender_name + 'sents a video';
						var body = '';
					}
					var message = {
						to: rowss[0].device_token,
						notification: {
							title: title, 
							body: body
						},
					    data: {
					    	"sender_id" : data.sender_id,
		                    "sender_name" : data.sender_name,
							"receiver_name" : rowss[0].name,
							"room_id" : data.room_id,
							"chat_type" : 'private',
							"type" : data.sender_type,
							"notification_type" : 'Private chat'
		                }
					};
					fcm.send(message,function(err,response){
					    if(err)
					       	console.log(err)
					    else
					    	console.log('elseeeeeeee')     
					})
				}
			})
		})
	})

	socket.on('get_chat_history',function(data){
		var final_result = {};
		if(data.chat_type == 'group'){
			var query = "SELECT * from group_messages where group_id = '" + data.room_id + "' ";
			var group_id = data.room_id
		}

		if(data.chat_type == 'private'){
			var query = "SELECT message_id,sender_id,sender_type,receiver_id,message,message_type,video_thumbnail,created_at from private_messages where room_id = '" + data.room_id + "' ";
			var group_id = ''
		}

		con.query(query,function(err,rows){
			if(err)
				throw err;

			var final_data = [];
            var i = 0;
            async.mapSeries(rows,function(new_data,callback){
				if(new_data.sender_type == 'client')
					var query2 = "SELECT name,profile_image from users where id = '" + new_data.sender_id + "' ";
				else
					var query2 = "SELECT name,profile_image from trainers where id = '" + new_data.sender_id + "' ";

				con.query(query2,function(err,user_data){
					if(err)
						throw err;

					if(user_data != ''){
                		new_data.user_name = user_data[0].name
                		new_data.profile_image = user_data[0].profile_image
                	}
                	final_data[i] = new_data;
                	final_result.data = final_data;
                	final_result.room_id = data.room_id;
                	final_result.user_id = data.user_id;
                	final_result.group_id = group_id;
                	final_result.type = data.type;
				    i++;
                	return callback(null,final_result);
				})
			},function(err,results){
                io.sockets.emit('chat_history',final_result);
            });
		})
	})

	socket.on('delete_message',function(data){
		if(data.chat_type == 'group'){
			var select_query = "SELECT sender_id, sender_type from group_messages where message_id = '" + data.message_id + "' ";
			var delete_query = "DELETE from group_messages where message_id = '" + data.message_id + "' ";
		}
		else{
			var select_query = "SELECT sender_id, sender_type from private_messages where message_id = '" + data.message_id + "' ";
			var delete_query = "DELETE from private_messages where message_id = '" + data.message_id + "' ";
		}

		con.query(select_query,function(err,rows){
			if(err)
				throw err;

			con.query(delete_query,function(err,result){
				if(err)
					throw err;

				var response_data = {
					"sender_id" : rows[0].sender_id,
					"sender_type" : rows[0].sender_type,
					"message_id" : data.message_id,
					"chat_type" : data.chat_type
				}
				io.sockets.emit('message_deleted', response_data)
			})
		})
	})

	socket.on('delete_group_from_list',function(data){
		con.query("DELETE FROM group_members WHERE group_id = '" + data.group_id + "' and member_id = '" + data.member_id + "' ",function(err,result){
            if(err)
                throw err;

            var response_data = {
				"id" : data.group_id
			}
            io.sockets.emit('group_deleted_from_list', response_data)
        });
	})

	socket.on('get_rooms_linked_with_user',function(data){
		var final_result = {};
		if(data.user_type == 'trainer'){
			//Trainer side private chat data
			
			console.log('SELECT delete_rooms.chat_id as chat_id from delete_rooms inner join private_messages on delete_rooms.chat_id = private_messages.room_id where delete_rooms.chat_type = "private" and delete_rooms.user_id = "' + data.user_id + '" and delete_rooms.user_type = "trainer" && message_id IN (SELECT MAX(message_id) FROM private_messages GROUP BY room_id) && delete_rooms.deleted_at > private_messages.created_at group by chat_id')

			con.query('SELECT delete_rooms.chat_id as chat_id from delete_rooms inner join private_messages on delete_rooms.chat_id = private_messages.room_id where delete_rooms.chat_type = "private" and delete_rooms.user_id = "' + data.user_id + '" and delete_rooms.user_type = "trainer" && message_id IN (SELECT MAX(message_id) FROM private_messages GROUP BY room_id) && delete_rooms.deleted_at > private_messages.created_at group by chat_id',function(err,delete_rooms_rows){
				if(err)
					throw err;

				var chat_ids = [];
				for(var x=0; x<delete_rooms_rows.length;x++){
					chat_ids.push(delete_rooms_rows[x].chat_id);
					chat_ids.join(',')
				}
				var concat_chat_ids = chat_ids.toString()

				if(delete_rooms_rows == '')
					var query = 'SELECT room_id as chat_id, room_name FROM rooms WHERE room_name LIKE "'+ data.user_id + '\\_%" group by chat_id';
				else
					var query = 'SELECT room_id as chat_id, room_name FROM rooms WHERE room_name LIKE "'+ data.user_id + '\\_%" and room_id NOT in (' + concat_chat_ids + ')';

				con.query(query,function(err,rows){
					if(err)
						throw err;

					var final_data = [];
		        	var i = 0;
		        	async.mapSeries(rows, function(data,callback){
		        		var user_ids = data.room_name.split('_');
		        		con.query("SELECT name, profile_image from users where id = '" + user_ids[1] + "' ",function(err,user_rows){
		        			if(err)
		        				throw err;

		        			data.chat_name = user_rows[0].name;
		        			data.chat_image = user_rows[0].profile_image;
		        			con.query("SELECT message, created_at from private_messages where private_messages.room_id = '" + data.chat_id + "' order by message_id DESC limit 1 ",function(err,rows2){
			            		if(err)
			              			throw err;

			              		if(rows2 == ''){
			              			data.message = '';
			              			data.created_at = '';
			              			data.client_id = '';
			              		}else{
			              			data.message = rows2[0].message;
			              			data.created_at = rows2[0].created_at;
			              			data.client_id = user_ids[1];
			              		}
			              		data.chat_type = 'private';
			            		final_data[i]  = data;
			            		i++;
			            		return callback(null, final_data);
			          		});

		        		})
		        	},function(err,results){
		        		//Trainer side group chat data
		        		con.query('SELECT delete_rooms.chat_id as chat_id from delete_rooms inner join group_messages on delete_rooms.chat_id = group_messages.group_id where delete_rooms.chat_type = "group" and delete_rooms.user_id = "' + data.user_id + '" and delete_rooms.user_type = "trainer" && message_id IN (SELECT MAX(message_id) FROM group_messages GROUP BY group_id) && delete_rooms.deleted_at > group_messages.created_at group by chat_id',function(err,check_rows){
		        			if(err)
		        				throw err;

		        			var chat_id = [];
							for(var x=0; x<check_rows.length;x++){
								chat_id.push(check_rows[x].chat_id);
								chat_id.join(',')
							}
							var concat_chat_id = chat_id.toString()

							if(concat_chat_id == '')
								var queryy = 'SELECT group_messages.group_id as group_id, group_messages.group_id as chat_id, groups.name as chat_name, groups.image as chat_image from groups inner join group_messages on groups.id = group_messages.group_id where groups.creator_id = "' + data.user_id + '" group by group_id';
							else
								var queryy = 'SELECT group_messages.group_id as group_id, group_messages.group_id as chat_id, groups.name as chat_name, groups.image as chat_image from groups inner join group_messages on groups.id = group_messages.group_id where groups.creator_id = "' + data.user_id + '" and group_messages.group_id NOT IN (' + concat_chat_id + ') group by group_id';

							con.query(queryy,function(err,rows3){
								if(err)
									throw err;

					        	var j = 0;
					        	if(rows3.length){
					        		async.mapSeries(rows3, function(data2,callback){
						          		con.query("SELECT message, created_at from group_messages where group_id = '" + data2.chat_id + "' order by message_id DESC limit 1 ",function(err,rows4){
						            		if(err)
						              			throw err;

						              		data2.message = rows4[0].message;
						              		data2.created_at = rows4[0].created_at;
						              		data2.chat_type = 'group';
						            		final_data.push(data2);
						            		final_result.data = final_data;
						                	final_result.user_id = data.user_id;
						                	final_result.user_type = data.user_type;
						            		j++;
						            		return callback(null, final_result);
						          		});
						        	},function(err,results){
						        		if(!final_result.data)
						        			final_result = final_data;
						        		else
						        			final_result = final_result;

						        		io.sockets.emit('rooms_linked_with_user', final_result)
						        	});
					        	}else{
					        		final_result.data = final_data;
				                	final_result.user_id = data.user_id;
				                	final_result.user_type = data.user_type;
					        		io.sockets.emit('rooms_linked_with_user', final_result)
					        	}
							})
		        		})
		        	});
				})
			})
		}else{
			//Client side private chat data
			con.query('SELECT delete_rooms.chat_id as chat_id from delete_rooms inner join private_messages on delete_rooms.chat_id = private_messages.room_id where delete_rooms.chat_type = "private" and delete_rooms.user_id = "' + data.user_id + '" and delete_rooms.user_type = "client" && message_id IN (SELECT MAX(message_id) FROM private_messages GROUP BY room_id) && delete_rooms.deleted_at > private_messages.created_at group by chat_id',function(err,delete_rooms_rows){
				if(err)
					throw err;

				var chat_ids = [];
				for(var x=0; x<delete_rooms_rows.length;x++){
					chat_ids.push(delete_rooms_rows[x].chat_id);
					chat_ids.join(',')
				}
				var concat_chat_ids = chat_ids.toString()

				if(delete_rooms_rows == '')
					var query = 'SELECT room_id as chat_id, room_name FROM rooms WHERE room_name LIKE "%\\_'+ data.user_id + '" ';
				else
					var query = 'SELECT room_id as chat_id, room_name FROM rooms WHERE room_name LIKE "%\\_'+ data.user_id + '" and room_id NOT in (' + concat_chat_ids + ') ';

				con.query(query,function(err,rows){
					if(err)
						throw err;

					var final_data = [];
		        	var i = 0;
		        	async.mapSeries(rows, function(data,callback){
		        		var user_ids = data.room_name.split('_');
		        		con.query("SELECT name, profile_image from trainers where id = '" + user_ids[0] + "' ",function(err,user_rows){
		        			if(err)
		        				throw err;

		        			data.chat_name = user_rows[0].name;
		        			data.chat_image = user_rows[0].profile_image;
		        			con.query("SELECT message, created_at from private_messages where private_messages.room_id = '" + data.chat_id + "' order by message_id DESC limit 1 ",function(err,rows2){
		        				if(err)
			              			throw err;

			              		if(rows2 == ''){
			              			data.message = '';
			              			data.created_at = '';
			              			data.trainer_id = '';
			              		}else{
			              			data.message = rows2[0].message;
			              			data.created_at = rows2[0].created_at;
			              			data.trainer_id = user_ids[0];
			              		}
			              		data.chat_type = 'private';
			            		final_data[i]  = data;
			            		i++;
			            		return callback(null, final_data);
		        			})
		        		})
		        	},function(err,results){
		        		//Client side group chat data
		        		con.query('SELECT delete_rooms.chat_id as chat_id from delete_rooms inner join group_messages on delete_rooms.chat_id = group_messages.group_id where delete_rooms.chat_type = "group" and delete_rooms.user_id = "' + data.user_id + '" and delete_rooms.user_type = "client" && message_id IN (SELECT MAX(message_id) FROM group_messages GROUP BY group_id) && delete_rooms.deleted_at > group_messages.created_at group by chat_id',function(err,delete_rooms_rows2){
		        			if(err)
		        				throw err;

		        			var chat_ids2 = [];
							for(var x=0; x<delete_rooms_rows2.length;x++){
								chat_ids2.push(delete_rooms_rows2[x].chat_id);
								chat_ids2.join(',')
							}
							var concat_chat_ids2 = chat_ids2.toString();
		        			if(delete_rooms_rows2 == ''){
		        				var query = 'SELECT groups.id as group_id, groups.id as chat_id,groups.name as chat_name, groups.image as chat_image, group_messages.message, group_messages.created_at from group_members inner join groups on group_members.group_id = groups.id inner join group_messages on group_members.group_id = group_messages.group_id where group_members.member_id = "' + data.user_id + '" && message_id IN (SELECT MAX(message_id) FROM group_messages GROUP BY group_id) group by chat_id ';
		        			}else{
		        				var query = 'SELECT groups.id as group_id, groups.id as chat_id,groups.name as chat_name, groups.image as chat_image, group_messages.message, group_messages.created_at from group_members inner join groups on group_members.group_id = groups.id inner join group_messages on group_members.group_id = group_messages.group_id where group_members.member_id = "' + data.user_id + '" && message_id IN (SELECT MAX(message_id) FROM group_messages GROUP BY group_id) && groups.id NOT IN (' + concat_chat_ids2 + ') group by chat_id ';
		        			}

		        			con.query(query,function(err,rows3){
								if(err)
									throw err;

								for(var j=0; j< rows3.length; j++){
									rows3[j].chat_type = 'group';
									final_data.push(rows3[j])
								}
			            		final_result.data = final_data;
			                	final_result.user_id = data.user_id;
			                	final_result.user_type = data.user_type;

			            		io.sockets.emit('rooms_linked_with_user', final_result)
							})
		        		})
		        	});
				})
			})
		}
	})

	socket.on('user_last_access',function(data){
		var current_date = Math.floor(new Date().valueOf() / 1000);
		con.query("SELECT id from room_last_access where user_id = '" + data.user_id + "' and user_type = '" + data.user_type + "' and room_id = '" + data.room_id + "' and room_type = '" + data.room_type + "' and room_status = '" + data.room_status + "' ",function(err,rows){
			if(err)
				throw err;

			if(!rows.length){
				con.query("INSERT into room_last_access (room_id,room_type,room_status,user_id,user_type,last_access) values ('" + data.room_id + "', '" + data.room_type + "', '" + data.room_status + "', '" + data.user_id + "', '" + data.user_type + "', '" + current_date + "') ",function(err,result){
					if(err)
						throw err;

					io.sockets.emit('user_last_accessed', data)
				})
			}else{
				con.query("UPDATE room_last_access SET last_access = '" + current_date + "' where id = '" + rows[0].id + "' ",function(err,result){
					if(err)
						throw err;

					io.sockets.emit('user_last_accessed', data)
				})
			}
		})
	})

	socket.on('get_badge_count',function(data){
		if(data.user_type == 'client'){
			var request_query = "SELECT count(id) as request_count from client_trainer_relationship where user_id = '" + data.user_id + "' and status = 'Requested' and sender_type != 'client' ";
			var group_post_query = "SELECT room_last_access.room_id,room_last_access.last_access from room_last_access inner join groups on room_last_access.room_id = groups.id where room_last_access.user_id = '" + data.user_id + "' and room_last_access.user_type = 'client' and room_last_access.room_type = 'group' and room_last_access.room_status = 'post' and groups.is_deleted = 'no' ";
			var group_chat_query = "SELECT room_last_access.room_type as chat_type,room_last_access.room_id,room_last_access.last_access from room_last_access inner join groups on room_last_access.room_id = groups.id where room_last_access.user_id = '" + data.user_id + "' and room_last_access.user_type = 'client' and room_last_access.room_type = 'group' and room_last_access.room_status = 'chat' and groups.is_deleted = 'no' ";
			var private_chat_query = "SELECT room_id,last_access from room_last_access where user_id = '" + data.user_id + "' and user_type = 'client' and room_type = 'private' and room_status = 'chat' ";
		}else{
			var request_query = "SELECT count(id) as request_count from client_trainer_relationship where trainer_id = '" + data.user_id + "' and status = 'Requested' and sender_type != 'trainer' ";
			var group_post_query = "SELECT room_last_access.room_id,room_last_access.last_access from room_last_access inner join groups on room_last_access.room_id = groups.id where room_last_access.user_id = '" + data.user_id + "' and room_last_access.user_type = 'trainer' and room_last_access.room_type = 'group' and room_last_access.room_status = 'post' and groups.is_deleted = 'no' ";
			var group_chat_query = "SELECT room_last_access.room_type as chat_type,room_last_access.room_id,room_last_access.last_access from room_last_access inner join groups on room_last_access.room_id = groups.id where room_last_access.user_id = '" + data.user_id + "' and room_last_access.user_type = 'trainer' and room_last_access.room_type = 'group' and room_last_access.room_status = 'chat' and groups.is_deleted = 'no' ";
			var private_chat_query = "SELECT room_id,last_access from room_last_access where user_id = '" + data.user_id + "' and user_type = 'trainer' and room_type = 'private' and room_status = 'chat' ";
		}

		con.query(request_query,function(err,rows){
			if(err)
				throw err;

			con.query(group_post_query,function(err,rows3){
				if(err)
					throw err;

				var final_data = [];
	        	var i = 0;
	        	async.mapSeries(rows3, function(data2,callback){
	        		con.query("SELECT count(group_posts.id) as count from group_posts where group_id = '" + data2.room_id + "' and created_at > '" + data2.last_access + "' ",function(err,rows4){
	        			if(err)
	        				throw err;

	        			data2.count = rows4[0].count;
	        			final_data[i]  = data2;
		            	i++;
	        			return callback(null, final_data);
	        		})
	        	},function(err,results){
	        		con.query(group_chat_query,function(err,rows5){
	        			if(err)
	        				throw err;

	        			var final_data2 = [];
	        			var j = 0;
	        			async.mapSeries(rows5, function(data3,callback){
	        				con.query("SELECT count(group_messages.message_id) as count from group_messages where group_id = '" + data3.room_id + "' and created_at > '" + data3.last_access + "' ",function(err,rows6){
	        					if(err)
	        						throw err;

			        			data3.count = rows6[0].count;
			        			final_data2[j]  = data3;
				            	j++;
			        			return callback(null, final_data2);
	        				})
	        			},function(err,results){
	        				console.log(private_chat_query)
	        				con.query(private_chat_query,function(err,rows7){
	        					if(err)
	        						throw err;

	        					console.log('========',rows7)
	        					if(rows7.length){
	        						console.log('DATA EXISTTTTTT',data)
	        					}else{
	        						console.log("DATA DOES NOT EXISTTTTTT", data)
	        					}

	        					async.mapSeries(rows7, function(data4,callback){
	        						con.query("SELECT count(private_messages.message_id) as count from private_messages where room_id = '" + data4.room_id + "' and created_at > '" + data4.last_access + "' ",function(err,rows8){
	        							if(err)
	        								throw err;

	        							for(var z=0; z<rows8.length; z++){
	        								rows8[z].chat_type = 'private';
	        								rows8[z].room_id = data4.room_id
	        								final_data2.push(rows8[z])
	        							}
			        					return callback(null, final_data2);
	        						})
	        					},function(err,results){
	        						if(data.user_type == 'client'){
	        							con.query("SELECT count(id) as group_request_count from group_requests where client_id = '" + data.user_id + "' ",function(err,rows2){
				        					if(err)
				        						throw err;

				        					var response_data = {
												"request_count" : rows[0].request_count,
												"group_request_count" : rows2[0].group_request_count,
												"group_post_count" : final_data,
												"chat_count" : final_data2,
												"user_id" : data.user_id,
												"user_type" : data.user_type
											}
											io.sockets.emit('badge_count', response_data)
		        						})
	        						}else{
			        					var response_data = {
											"request_count" : rows[0].request_count,
											"group_post_count" : final_data,
											"chat_count" : final_data2,
											"user_id" : data.user_id,
											"user_type" : data.user_type
										}
										io.sockets.emit('badge_count', response_data)
	        						}
	        					})
	        				})
	        			})
	        		})
	        	});
			})
		})
	})
});

