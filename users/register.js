var con = require('../connection/connection');
var passwordHash = require('password-hash');
var sgMail = require('@sendgrid/mail');

module.exports.client_register = function(req,res){
    var validation_error = 'false';
	if(req.body.is_social == 'false'){
        if(req.body.email == '' || req.body.password == ''){
            validation_error = 'true';
            res.send({
                "status":0,
                "message":"Email or password cannot be empty"
            })
        }
    }else{
        if(req.body.email == ''){
            validation_error = 'true';
            res.send({
                "status":0,
                "message":"Email cannot be empty"
            })
        }
    }
    if(validation_error == 'false'){
        var created_at = Math.floor(new Date().valueOf() / 1000);
        var hashedPassword = passwordHash.generate(req.body.password);
        //check if email exist or not
        con.query('SELECT * FROM users WHERE email = "' + req.body.email + '" ',function(err,check_email){
            if(err){
                res.send({
                    "status":0,
                    "message":"There is an error while fetching user data wth email"
                })
            }else{
                if(!check_email.length){
                    con.query("INSERT INTO users (email, password, phone_number, device_type, device_token, is_social, social_id, social_type, created_at) VALUES ('" + req.body.email + "','" + hashedPassword + "', '" + req.body.phone_number + "', '" + req.body.device_type + "', '" + req.body.device_token + "', '" + req.body.is_social + "', '" + req.body.social_id + "', '" + req.body.social_type + "', '" + created_at + "')", function (err, user_result) {
                        if(err){
                            res.send({
                                "status":0,
                                "message":"error ocurred while inserting user data"
                            })
                        }else{
                            con.query("SELECT * from users where id = '" + user_result.insertId + "' ", function(err,user_data){
                                if(err){
                                    res.send({
                                        "status":0,
                                        "message":"error ocurred while fetching user data"
                                    })
                                }else{
                                    res.send({
                                        "status":1,
                                        "message":"User registered successfully",
                                        "data": user_data[0]
                                    })

                                    process.env.SENDGRID_API_KEY = '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';
                                    sgMail.setApiKey(process.env.SENDGRID_API_KEY);

                                    var msg = {
                                        to: req.body.email,
                                        from: 'no-reply@briana-fitness.com',
                                        templateId : 'd-2f0e6b72f33640df9ade644549bf3102',
                                        /*dynamic_template_data: {
                                            name: user_data[0].name
                                        }*/
                                    };
                                    sgMail.send(msg);
                                }
                            })
                        }
                    });
                }else{
                    //user with provided email already exist
                    res.send({
                        "status":0,
                        "message":"This e-mail is already used to sign up with us before!"
                    })
                }
            }
        });
    }
}

module.exports.trainer_register = function(req,res){
    var validation_error = 'false';
    if(req.body.is_social == 'false'){
        if(req.body.email == '' || req.body.password == ''){
            validation_error = 'true';
            res.send({
                "status":0,
                "message":"Email or password cannot be empty"
            })
        }
    }
   else{
        if(req.body.email == ''){
            validation_error = 'true';
            res.send({
                "status":0,
                "message":"Email cannot be empty"
            })
        }
    }
    if(validation_error == 'false'){
        var created_at = Math.floor(new Date().valueOf() / 1000);
        var hashedPassword = passwordHash.generate(req.body.password);
        //check if email exist or not
        con.query('SELECT * FROM trainers WHERE email = "' + req.body.email + '" ',function(err,check_email){
            if(err){
                res.send({
                    "status":0,
                    "message":"There is an error while fetching trainer data with email"
                })
            }else{
                if(!check_email.length){
                    con.query("INSERT INTO trainers (email, password, device_type, device_token, is_social, social_id, social_type, created_at) VALUES ('" + req.body.email + "','" + hashedPassword + "', '" + req.body.device_type + "', '" + req.body.device_token + "', '" + req.body.is_social + "', '" + req.body.social_id + "', '" + req.body.social_type + "', '" + created_at + "')", function (err, user_result) {
                        if(err){
                            res.send({
                                "status":0,
                                "message":"error ocurred while inserting user data"
                            })
                        }else{
                            con.query("SELECT * from trainers where id = '" + user_result.insertId + "' ", function(err,trainer_data){
                                if(err){
                                    res.send({
                                        "status":0,
                                        "message":"error ocurred while fetching user data"
                                    })
                                }else{
                                    res.send({
                                        "status":1,
                                        "message":"User registered successfully",
                                        "data": trainer_data[0]
                                    })

                                    process.env.SENDGRID_API_KEY = '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';
                                    sgMail.setApiKey(process.env.SENDGRID_API_KEY);

                                    var msg = {
                                        to: req.body.email,
                                        from: 'no-reply@briana-fitness.com',
                                        templateId : 'd-2f0e6b72f33640df9ade644549bf3102',
                                        /*dynamic_template_data: {
                                            name: user_data[0].name
                                        }*/
                                    };
                                    sgMail.send(msg);
                                }
                            })
                        }
                    });
                }else{
                    //user with provided email already exist
                    res.send({
                        "status":0,
                        "message":"This e-mail is already used to sign up with us before!"
                    })
                }
            }
        });
    }
}

