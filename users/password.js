var con = require('../connection/connection');
var passwordHash = require('password-hash');
var sgMail = require('@sendgrid/mail');
var randomstring = require("randomstring");

module.exports.change_password = function(req,res){

    if(req.body.old_password == '' || req.body.new_password == ''){
        res.send({
            "status":0,
            "message":"Fields cannot be empty"
        })
    }else{
        var hashedPassword = passwordHash.generate(req.body.new_password);
        if(req.body.type == 'client'){
            var select_query = 'SELECT * from users where id = "' + req.body.user_id + '" ';
            var update_query = "UPDATE users SET password = '" + hashedPassword + "' WHERE id = '" + req.body.user_id + "' ";
        }else{
            var select_query = 'SELECT * from trainers where id = "' + req.body.user_id + '" ';
            var update_query = "UPDATE trainers SET password = '" + hashedPassword + "' WHERE id = '" + req.body.user_id + "' ";
        }
        con.query(select_query,function(err,user_rows){
            if(err){
                res.send({
                    "status":0,
                    "message":"There is an error while fetching user data"
                });
            }else{
                if(!user_rows.length){
                    res.send({
                        "status":0,
                        "message":"User does not exist"
                    });
                }else{
                    var checkPass = passwordHash.verify(req.body.old_password, user_rows[0].password);
                    if(checkPass == true){
                        con.query(update_query, function (err,user_results){
                            if(err){
                                res.send({
                                    "status":0,
                                    "message":"error ocurred while updating new password"
                                })
                            }else{
                                res.send({
                                    "status":1,
                                    "message":"Password updated successfully",
                                    "data" : user_rows[0]
                                });
                                process.env.SENDGRID_API_KEY = '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';
                                sgMail.setApiKey(process.env.SENDGRID_API_KEY);

                                var msg = {
                                    to: user_rows[0].email,
                                    from: 'no-reply@briana-fitness.com',
                                    templateId : 'd-fdd7f981d55e4003a60f38d183268549',
                                };
                                sgMail.send(msg);
                            }
                        });
                    }else{
                        res.send({
                            "status":0,
                            "message":"Old password does not match"
                        });
                    }
                }
            }
        });
    }
}

module.exports.forgot_password = function(req,res){
    if(req.body.email == '' || req.body.type == ''){
        res.send({
            "status":0,
            "message":"Fields cannot be empty"
        })
    }else{
        var random_password = randomstring.generate(8);
        var hashedPassword = passwordHash.generate(random_password);

        if(req.body.type == 'client'){
            var select_query = 'SELECT * from users where email = "' + req.body.email + '" ';
            var update_query = "UPDATE users SET forgot_password_token = '" + hashedPassword + "' WHERE email = '" + req.body.email + "' ";
        }
        else{
            var select_query = 'SELECT * from trainers where email = "' + req.body.email + '" ';
            var update_query = "UPDATE trainers SET forgot_password_token = '" + hashedPassword + "' WHERE email = '" + req.body.email + "' ";
        }
        con.query(select_query,function(err,user_rows){
            if(err){
                res.send({
                    "status":0,
                    "message":"There is an error while fetching user data"
                })
            }else{
               if(!user_rows.length){
                    res.send({
                        "status":0,
                        "message":"Email does not exist"
                    });
                }else{
                    process.env.SENDGRID_API_KEY = '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';
                    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                    var reset_link = 'http://ec2-3-17-219-208.us-east-2.compute.amazonaws.com:8001/forgot_password?type=' + req.body.type + '&token=' + hashedPassword + ' ';

                    var msg = {
                        to: req.body.email,
                        from: 'no-reply@briana-fitness.com',
                        templateId : 'd-12cc6b258356416a82dba617f6f2fce3',
                        dynamic_template_data: {
                            link: reset_link
                        }
                    };
                    sgMail.send(msg);

                    con.query(update_query, function(err,results){
                        if(err){
                            res.send({
                                "status":0,
                                "message":"error ocurred while updating data"
                            })
                        }else{
                            res.send({
                                "status":1,
                                "message":"A reset password link is sent to your email-id"
                            });
                        }
                    });
                }
            }
        });
    }
}
