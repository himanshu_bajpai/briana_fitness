var con = require('../connection/connection');
var passwordHash = require('password-hash');

exports.login = function(req,res){
    if(req.body.email == '' || req.body.password == '' || req.body.type == ''){
        res.send({
            "status":0,
            "message":"Fields cannot be empty"
        })
    }else{
        if(req.body.type == 'client')
            var query = "SELECT * FROM users where email = '" + req.body.email + "' ";
        else
            var query = "SELECT * FROM trainers where email = '" + req.body.email + "' ";

        con.query(query, function(err, user_rows){
            if(err){
                res.send({
                    "status":0,
                    "message":"There is an error while fetching user data wth email"
                })
            }else{
                if(!user_rows.length){
                    res.send({
                        "status":0,
                        "message":"Email does not exist"
                    })
                }else{
                    var checkPass = passwordHash.verify(req.body.password, user_rows[0].password);
                    if(checkPass == true){
                        if(req.body.type == 'client')
                            var update_query = "UPDATE users SET device_token = '" + req.body.device_token + "', device_type = '" + req.body.device_type + "' where id = '" + user_rows[0].id + "' ";
                        else
                            var update_query = "UPDATE trainers SET device_token = '" + req.body.device_token + "', device_type = '" + req.body.device_type + "' where id = '" + user_rows[0].id + "' ";

                        con.query(update_query, function (err,user_result){
                            if(err){
                                res.send({
                                    "status":0,
                                    "message":"error ocurred while updating login data"
                                })
                            }else{
                                if(user_rows[0].name != ''){
                                    var profile_status = 'true';
                                    var message = "User logged in successfully";
                                }
                                else{
                                    var profile_status = 'false';
                                    var message = "Profile not setup";
                                }
                                res.send({
                                    "status":1,
                                    "message":message,
                                    "data" : user_rows[0],
                                    "profile_status" : profile_status
                                });
                            }
                        });
                    }else{
                        res.send({
                            "status":0,
                            "message":"Password does not match"
                        });
                    }
                }
            }
        })
    }
}

exports.social_login = function(req,res){
    if(req.body.type == 'client')
        var query = "SELECT * from users where social_id = '" + req.body.social_id + "' ";
    else
        var query = "SELECT * from trainers where social_id = '" + req.body.social_id + "' ";

    con.query(query, function (err,user_rows){
        if(err){
            res.send({
                "status":0,
                "message":"There is an error while fetching user data wth social id"
            })
        }else{
            if(!user_rows.length){
                res.send({
                    "status":0,
                    "message":"Social id does not exist"
                });
            }else{
                if(req.body.type == 'client')
                    var update_query = "UPDATE users SET device_token = '" + req.body.device_token + "', device_type = '" + req.body.device_type + "' where id = '" + user_rows[0].id + "' ";
                else
                    var update_query = "UPDATE trainers SET device_token = '" + req.body.device_token + "', device_type = '" + req.body.device_type + "' where id = '" + user_rows[0].id + "' "
                con.query(update_query, function (err,user_result){
                    if(err){
                        res.send({
                            "status":0,
                            "message":"error ocurred while updating social login data"
                        })
                    }else{
                        if(user_rows[0].name != ''){
                            var profile_status = 'true';
                            var message = "User logged in successfully";
                        }
                        else{
                            var profile_status = 'false';
                            var message = "Profile not setup";
                        }
                        res.send({
                            "status":1,
                            "message": message,
                            "data": user_rows[0],
                            "profile_status" : profile_status
                        });
                    }
                });
            }
        }
    })
}

