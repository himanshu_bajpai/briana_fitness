var con = require('../connection/connection');

module.exports.get_profile = function(req,res){
    if(req.body.type == 'client')
        var query = "SELECT users.id, users.name, users.email, users.phone_number, users.profile_image, users.state, users.country, users.profile_privacy, users.fitness_level, users.activity_factor, users.weight, users.height, users.dob, users.gender, users.user_bio, users.user_goal, users.other_goal, count(client_trainer_relationship.id) as trainer_count from users inner join client_trainer_relationship on users.id = client_trainer_relationship.user_id where users.id = '" + req.body.user_id + "' and client_trainer_relationship.user_id = '" + req.body.user_id + "' and client_trainer_relationship.status = 'Accepted' ";
    else
        var query = "SELECT trainers.id, trainers.name, trainers.email, trainers.phone_number, trainers.profile_image, trainers.weight, trainers.height, trainers.city_state, trainers.age, trainers.gender, count(groups.id) as group_count from trainers inner join groups on trainers.id = groups.creator_id  where trainers.id = '" + req.body.user_id + "' and groups.creator_id = '" + req.body.user_id + "' and groups.is_deleted = 'no' ";

    con.query(query,function (err,profile_rows){
        if(err){
            res.send({
                "status":0,
                "message":"There is an error while fetching user profile data"
            });
        }else{
            if(!profile_rows.length){
                res.send({
                    "status":0,
                    "message":"User doesnot exist"
                });
            }else{
            	if(req.body.type == 'trainer'){
            		con.query("SELECT count(id) as client_count from client_trainer_relationship where trainer_id = '" + req.body.user_id + "' and status = 'Accepted' ",function(err,rows){
            			if(err){
            				res.send({
				                "status":0,
				                "message":"There is an error while fetching user profile data"
				            });
            			}else{
            				profile_rows[0].client_count = rows[0].client_count;
            				res.send({
			                    "status":1,
			                    "message":"User exist",
			                    "data" : profile_rows[0]
			                });
            			}
            		})
            	}else{
            		res.send({
	                    "status":1,
	                    "message":"User exist",
	                    "data" : profile_rows[0]
	                });
            	}
            }
        }
  	});
}

module.exports.client_edit_profile = function(req,res){
    var height_ext = req.body.height.split(' ');
    if(height_ext[1] == 'FT')
        var query = 'UPDATE users SET email = "' + req.body.email + '", phone_number = "' + req.body.phone_number + '", profile_image = "' + req.body.profile_image + '",name = "' + req.body.name + '", country = "' + req.body.country + '", state = "' + req.body.state + '", profile_privacy = "' + req.body.profile_privacy + '", weight = "' + req.body.weight + '", height = "' + req.body.height + '", dob = "' + req.body.dob + '", gender = "' + req.body.gender + '", fitness_level = "' + req.body.fitness_level + '", activity_factor = "' + req.body.activity_factor + '", user_bio = "' + req.body.user_bio + '", user_goal = "' + req.body.user_goal + '", other_goal = "' + req.body.other_goal + '" where id = "' + req.body.user_id + '" ';
    else
        var query = "UPDATE users SET email = '" + req.body.email + "', phone_number = '" + req.body.phone_number + "', profile_image = '" + req.body.profile_image + "',name = '" + req.body.name + "', country = '" + req.body.country + "', state = '" + req.body.state + "', profile_privacy = '" + req.body.profile_privacy + "', weight = '" + req.body.weight + "', height = '" + req.body.height + "', dob = '" + req.body.dob + "', gender = '" + req.body.gender + "', fitness_level = '" + req.body.fitness_level + "', activity_factor = '" + req.body.activity_factor + "', user_bio = '" + req.body.user_bio + "', user_goal = '" + req.body.user_goal + "', other_goal = '" + req.body.other_goal + "' where id = '" + req.body.user_id + "' ";
    con.query(query, function (err, edit_rows) {
        if(err){
            res.send({
                "status":0,
                "message":"There is an error while updating client profile data"
            });
        }else{
            res.send({
                "status":1,
                "message":"Client profile updated successfully"
            });
        }
    });
}

module.exports.trainer_edit_profile = function(req,res){
    var height_ext = req.body.height.split(' ');
    if(height_ext[1] == 'FT')
        var query = 'UPDATE trainers SET email = "' + req.body.email + '", phone_number = "' + req.body.phone_number + '", profile_image = "' + req.body.profile_image + '", name = "' + req.body.name + '", city_state = "' + req.body.city_state + '", weight = "' + req.body.weight + '", height = "' + req.body.height + '", birthday = "' + req.body.birthday + '", gender = "' + req.body.gender + '", age = "' + req.body.age + '" where id = "' + req.body.user_id + '" ';
    else
        var query = "UPDATE trainers SET email = '" + req.body.email + "', phone_number = '" + req.body.phone_number + "', profile_image = '" + req.body.profile_image + "', name = '" + req.body.name + "', city_state = '" + req.body.city_state + "', weight = '" + req.body.weight + "', height = '" + req.body.height + "', birthday = '" + req.body.birthday + "', gender = '" + req.body.gender + "', age = '" + req.body.age + "' where id = '" + req.body.user_id + "' ";

    con.query(query, function (err, edit_rows) {
        if(err){
            res.send({
                "status":0,
                "message":"There is an error while updating trainer profile data"
            });
        }else{
            res.send({
                "status":1,
                "message":"Trainer profile updated successfully"
            });
        }
    });
}

