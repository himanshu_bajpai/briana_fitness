<?php

class WD_CI_Model extends CI_Model{

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function wd_result($record){
        echo json_encode($record);
        exit;
    }

    public function make_needed_directories($directory){
        if (!is_dir($directory)) {
            mkdir($directory, 0777, TRUE);
        }
        return 1;
    }

    function check_admin(){
        $this->db->select('*');
        $this->db->from('admin_login');
        $result = $this->db->get()->row();
        return $result;
    }

    function add_category($data){
        if($query = $this->db->insert('categories', $data)){
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }else{
            return false;
        }
    }

    function add_sub_category($data){
        if($query = $this->db->insert('sub_categories', $data)){
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }else{
            return false;
        }
    }

    function get_categories(){
        $this->db->select('*');
        $this->db->from('categories');
        $result = $this->db->get()->result();
        return $result;
    }

    function get_sub_categories(){
        $this->db->select('sub_categories.*');
        $this->db->select('categories.category_name');
        $this->db->from('sub_categories');
        $this->db->join('categories', 'sub_categories.category_id = categories.category_id', 'INNER');
        $result = $this->db->get()->result();
        return $result;
    }

    function get_categories_with_sub_categories_count(){
        $this->db->select('*');
        $this->db->from('categories');
        $result = $this->db->get()->result();
        if($result){
            foreach($result as $result_key => $row){
                $this->db->select('count(category_id) as count');
                $this->db->from('sub_categories');
                $this->db->where(['category_id' => $row->category_id]);
                $result2 = $this->db->get()->row();
                $result[$result_key]->count = $result2->count;
            }
            return $result;
        }else
            return false;
    }

    function get_trainers(){
        $this->db->select('*');
        $this->db->from('trainers');
        $result = $this->db->get()->result();
        return $result;
    }

    function get_clients(){
        $this->db->select('*');
        $this->db->from('users');
        $result = $this->db->get()->result();
        return $result;
    }

    function get_subscription_packs(){
        $this->db->select('*');
        $this->db->from('subscription_packs');
        $result = $this->db->get()->result();
        return $result;
    }

    function add_subscription_pack($data){
        if($query = $this->db->insert('subscription_packs', $data)){
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }else{
            return false;
        }
    }

    function get_exercises(){
        $this->db->select('exercises.*');
        $this->db->select('categories.category_name');
        $this->db->select('sub_categories.sub_category_name');
        $this->db->from('exercises');
        $this->db->join('categories', 'exercises.category_id = categories.category_id', 'INNER');
        $this->db->join('sub_categories', 'exercises.sub_category_id = sub_categories.sub_category_id', 'INNER');
        $result = $this->db->get()->result();
        return $result;
    }

    function add_exercise($data){
        if($query = $this->db->insert('exercises', $data)){
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }else{
            return false;
        }
    }

    function get_categories_based_id($category_id){
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where(['category_id' => $category_id]);
        $result = $this->db->get()->row();
        return $result;
    }

    function get_sub_categories_based_on_category($category_id){
        $this->db->select('*');
        $this->db->from('sub_categories');
        $this->db->where(['category_id' => $category_id]);
        $result = $this->db->get()->result();
        return $result;
    }

    function get_trainers_count(){
        $this->db->select('count(*) as count');
        $this->db->from('trainers');
        $result = $this->db->get()->row();
        return $result;
    }

    function get_clients_count(){
        $this->db->select('count(*) as count');
        $this->db->from('users');
        $result = $this->db->get()->row();
        return $result;
    }

    function get_fitness_levels(){
        $this->db->select('*');
        $this->db->from('fitness_levels');
        $result = $this->db->get()->result();
        return $result;
    }

    function add_fitness_level($name){
        $data = array('name'=>$name);
        if($query = $this->db->insert('fitness_levels', $data)){
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }else{
            return false;
        }
    }

    function update_fitness_level($post_data){
        $data=array('name'=>$post_data['name']);
        $this->db->where('id',$post_data['id']);
        $this->db->update('fitness_levels',$data);
        return true;
    }

    function delete_fitness_level($get_data){
        $this->db->where('id', $get_data);
        $this->db->delete('fitness_levels');
        return true;
    }

    function get_trainer_based_ID($id){
        $this->db->select('*');
        $this->db->from('trainers');
        $this->db->where(['id' => $id]);
        $result = $this->db->get()->result();
        return $result;
    }

    function update_category($post_data){
        if(count($post_data) < 4)
            $data=array('category_name'=>$post_data['category_name']);
        else
            $data=array('category_name'=>$post_data['category_name'],'category_image'=>$post_data['category_image']);
        $this->db->where('category_id',$post_data['category_id']);
        $this->db->update('categories',$data);
        return true;
    }

    function delete_category($get_data){
        $this->db->where('category_id', $get_data);
        $this->db->delete('categories');
        return true;
    }

    function update_sub_category($post_data){
        if(count($post_data) < 4)
            $data=array('sub_category_name'=>$post_data['sub_category_name']);
        else
            $data=array('sub_category_name'=>$post_data['sub_category_name'],'sub_category_image'=>$post_data['sub_category_image']);
        $this->db->where('sub_category_id',$post_data['sub_category_id']);
        $this->db->update('sub_categories',$data);
        return true;
    }

    function delete_sub_category($get_data){
        $this->db->where('sub_category_id', $get_data);
        $this->db->delete('sub_categories');
        return true;
    }

    function get_trainer_based_clients($id){
        $this->db->select('users.name, users.email, users.gender, users.phone_number, users.profile_image, users.state');
        $this->db->from('client_trainer_relationship');
        $this->db->join('users', 'users.id = client_trainer_relationship.user_id', 'INNER');
        $this->db->where(['client_trainer_relationship.trainer_id' => $id, 'client_trainer_relationship.status' => 'Accepted']);
        $result = $this->db->get()->result();
        return $result;
    }

    function update_exercise($post_data){
        $this->db->where('exercise_id',$post_data['exercise_id']);
        $this->db->update('exercises',$post_data);
    }

    function delete_exercise($get_data){
        $this->db->where('exercise_id', $get_data);
        $this->db->delete('exercises');
        return true;
    }

    function get_exercise_data($exercise_id){
        $this->db->select('reps,sets,rest,exercise_video,exercise_image');
        $this->db->from('exercises');
        $this->db->where(['exercise_id' => $exercise_id]);
        $result = $this->db->get()->row();
        return $result;
    }

    function create_workout($post_data){
        $this->db->trans_start();
        $data = array( 
            'workout_name'=>$post_data['workout_name'], 
            'workout_description'=>$post_data['workout_description'], 
            'workout_category'=>$post_data['workout_category']
        );
        $this->db->insert('workout',$data);
        $workout_insert_id = $this->db->insert_id();

        $exercises = [];
        foreach($post_data['sec_name'] as $key => $sec_data){
            $data2 = array( 
                'workout_id'=>$workout_insert_id, 
                'section_name'=>$sec_data
            );
            $this->db->insert('workout_sections',$data2);

            foreach($post_data['reps_name'][$key] as $k => $v){
                $e_name = explode('~', $post_data['ex_name'][$key][$k]);
                $exercises[$key][] = [
                    'workout_exercise_name' => end($e_name),
                    'workout_exercise_reps' => $v,
                    'workout_exercise_sets' => $post_data['sets_name'][$key][$k],
                    'workout_exercise_rest' => $post_data['rest_name'][$key][$k],
                    'workout_notes' => $post_data['workout_notes'][$key][$k],
                    'section_id' => $this->db->insert_id(),
                    'workout_exercise_video' => $post_data['video_path'][$key][$k],
                    'workout_exercise_image' => $post_data['image_path'][$key][$k]
                ];
            }
            $this->db->insert_batch('workout_exercises',$exercises[$key]);
        }
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }

    function get_workout_list(){
        $this->db->select('workout.*,categories.category_name');
        $this->db->from('workout');
        $this->db->join('categories', 'workout.workout_category = categories.category_id', 'INNER');
        $result = $this->db->get()->result();
        return $result;
    }

    function get_workout_details($id){
        $this->db->select('*');
        $this->db->from('workout');
        $this->db->where(['workout_id' => $id]);
        $result = $this->db->get()->row();
        return $result;
    }

    function get_workout_section_data($id){
        $this->db->select('section_id,section_name');
        $this->db->from('workout_sections');
        $this->db->where(['workout_id' => $id]);
        $result = $this->db->get()->result();
        if($result){
            foreach($result as $result_key => $row){
                $this->db->select('workout_exercise_id, workout_exercise_name, workout_exercise_reps, workout_exercise_sets, workout_exercise_rest,workout_notes,workout_exercise_video');
                $this->db->from('workout_exercises');
                $this->db->where(['section_id' => $row->section_id]);
                $result2 = $this->db->get()->result();
                $result[$result_key]->exercise_data = $result2;
            }
            return $result;
        }else
            return false;
    }

    function delete_workout($get_data){
        $query = $this->db->query("DELETE workout, workout_sections, workout_exercises FROM workout, workout_sections, workout_exercises 
        WHERE workout.workout_id = workout_sections.workout_id 
        AND workout_sections.section_id = workout_exercises.section_id
        AND workout.workout_id= $get_data");
        if($query)
            return true;
        else
            return false;
    }

    function update_password($data){
        $this->db->where('forgot_password_token',$data['token']);
        $update_data=array('forgot_password_token'=>'');
        if($data['type'] == 'client')
            $this->db->update('users',$update_data);
        else
            $this->db->update('trainers',$update_data);
    }
}