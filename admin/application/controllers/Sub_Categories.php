<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_categories extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		if(isset($_POST['update'])){
			if($_FILES['sub_category_image']['name']){
				$wd_ci_model->make_needed_directories('./uploads/sub_categories');
                $config['upload_path'] = './uploads/sub_categories';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = TRUE;
                $config['file_name'] = date('U') . '_' . $_FILES['sub_category_image']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('sub_category_image')){
                    $data['sub_category_id'] = $_POST['sub_category_id'];
                    $data['sub_category_name'] = $_POST['sub_category_name'];
                    $data['sub_category_image'] = $this->upload->data()["file_name"];
                    $data['key'] = 'update';
                    $wd_ci_model->update_sub_category($data);
                }
			}else{
				$wd_ci_model->update_sub_category($_POST);
			}
		}
		if($_GET){
			$wd_ci_model->delete_sub_category($_GET['id']);
			header('Location:' . base_url() . 'index.php/sub_categories');
		}
		$get_sub_categories = $wd_ci_model->get_sub_categories();
		$head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
		$this->load->view('sub_categories',['get_sub_categories' => $get_sub_categories,'head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}
}