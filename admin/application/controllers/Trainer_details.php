<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trainer_details extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		$id = $_GET['id'];
        $get_trainer_details = $wd_ci_model->get_trainer_based_ID($id);
        $head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
        $get_trainer_based_clients = $wd_ci_model->get_trainer_based_clients($id);
        $this->load->view('trainer_details',['trainer_details' => $get_trainer_details,'head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer, 'get_trainer_based_clients' => $get_trainer_based_clients]);
	}
}