<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Workout_template extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		$head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
        $get_workout_list = $wd_ci_model->get_workout_list();
		$this->load->view('workout_template',['head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer, 'workout_list' => $get_workout_list]);
	}
}