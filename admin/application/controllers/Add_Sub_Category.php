<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_sub_category extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		if($_POST){
			if($_FILES['sub_category_image']['error'] > 0){
                $this->session->set_flashdata('error_sub_category', 'Sub category image is required');
            }else{
            	$wd_ci_model->make_needed_directories('./uploads/sub_categories/');
                $config['upload_path'] = './uploads/sub_categories/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = TRUE;
                $config['file_name'] = date('U') . '_' . $_FILES['sub_category_image']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $data['sub_category_name'] = $_POST['sub_category_name'];
                $data['category_id'] = $_POST['category_id'];
                if($this->upload->do_upload('sub_category_image')){
                    $data['sub_category_image'] = $this->upload->data()["file_name"];
                    $res = $wd_ci_model->add_sub_category($data);
                    header('Location:' . base_url() . 'index.php/sub_categories');
                }
                else
                    $this->session->set_flashdata('error_sub_category', strip_tags($this->upload->display_errors()));
            }
		}
        $head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
        $get_categories = $wd_ci_model->get_categories();
		$this->load->view('add_sub_category',['get_categories' => $get_categories,'head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}
}