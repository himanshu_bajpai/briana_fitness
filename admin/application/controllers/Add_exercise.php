<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_exercise extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		if($_POST){
			if($_FILES['exercise_image']['error'] > 0){
                $this->session->set_flashdata('error_exercise', 'Exercise image is required');
            }else if($_FILES['exercise_video']['error'] > 0){
                $this->session->set_flashdata('error_exercise', 'Exercise video is required');
            }else{
                $error_count = 0;
                $error_message = '';
                if($_FILES['exercise_video']){
                    $wd_ci_model->make_needed_directories('./uploads/exercises/videos');
                    $config['upload_path'] = './uploads/exercises/videos';
                    $config['allowed_types'] = 'mov|avi|flv|wmv|3gp|mp4';
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = date('U') . '_' . $_FILES['exercise_video']['name'];
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if($this->upload->do_upload('exercise_video'))
                        $data['exercise_video'] = $this->upload->data()["file_name"];
                    else{
                        $error_count = $error_count + 1;
                        $error_message = 'Video:' . strip_tags($this->upload->display_errors());
                    }
                }
                if($_FILES['exercise_image']){
                    $wd_ci_model->make_needed_directories('./uploads/exercises/images');
                    $config['upload_path'] = './uploads/exercises/images';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = date('U') . '_' . $_FILES['exercise_image']['name'];
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if($this->upload->do_upload('exercise_image'))
                        $data['exercise_image'] = $this->upload->data()["file_name"];
                    else{
                        $error_count = $error_count + 1;
                        $error_message = 'Image:' . strip_tags($this->upload->display_errors());
                    }
                        
                }
                if($error_count < 1){
                    $data['exercise_name'] = $_POST['exercise_name'];
                    $data['exercise_description'] = $_POST['exercise_description'];
                    $data['category_id'] = $_POST['category_id'];
                    $data['sub_category_id'] = $_POST['sub_category_id'];
                    $data['sets'] = $_POST['sets'];
                    $data['reps'] = $_POST['reps'];
                    $data['rest'] = $_POST['rest'];
                    $res = $wd_ci_model->add_exercise($data);
                    header('Location:' . base_url() . 'index.php/exercises');
                }else{
                    $this->session->set_flashdata('error_exercise', $error_message);
                }
            }
		}
        $get_categories = $wd_ci_model->get_categories();
        $head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
        $this->load->view('add_exercise',['categories' => $get_categories,'head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}

    function get_sub_categories_based_on_category(){
        $wd_ci_model = new WD_CI_Model();
        $category_id = $this->input->post();
        $sub_categories = $wd_ci_model->get_sub_categories_based_on_category($category_id['category_id']);
        if($sub_categories){
            foreach($sub_categories as $result_key => $row){
                echo "<option value='".$row->sub_category_id."'>".$row->sub_category_name."</option>";
            }
        }
    }
}