<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trainers extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
        $get_trainers = $wd_ci_model->get_trainers();
        $head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
        $this->load->view('trainers',['get_trainers' => $get_trainers, 'head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}
}
