<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_category extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		if($_POST){
			if($_FILES['category_image']['error'] > 0){
                $this->session->set_flashdata('error_category', 'Category image is required');
            }else{
            	$wd_ci_model->make_needed_directories('./uploads/categories/');
                $config['upload_path'] = './uploads/categories/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = TRUE;
                $config['file_name'] = date('U') . '_' . $_FILES['category_image']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $data['category_name'] = $_POST['category_name'];
                if($this->upload->do_upload('category_image')){
                    $data['category_image'] = $this->upload->data()["file_name"];
                    $res = $wd_ci_model->add_category($data);
                    header('Location:' . base_url() . 'index.php/categories');
                }
                else
                    $this->session->set_flashdata('error_category', strip_tags($this->upload->display_errors()));
            }
		}
        $head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
		$this->load->view('add_category',['head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}
}