<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		$trainers_count = $wd_ci_model->get_trainers_count();
		$clients_count = $wd_ci_model->get_clients_count();
		$head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
		$this->load->view('dashboard',['trainers_count' => $trainers_count->count, 'clients_count' => $clients_count->count, 'head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}
}