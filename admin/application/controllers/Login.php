<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		if($_POST){
			$data = $_POST;
            $check_admin = $wd_ci_model->check_admin();
            if($data['email'] == $check_admin->email){
            	if($data['password'] == $check_admin->password){
            		$this->session->set_userdata('email', $data['email']);
                    $this->session->set_userdata('password', $data['password']);
                    header('Location:' . base_url() . 'index.php/dashboard');
            	}else{
            		$this->session->set_flashdata('error_login', 'Password does not match');
            	}
            }else{
            	$this->session->set_flashdata('error_login', 'Email does not exist');
            }
		}
		$this->load->view('login');
	}
}
