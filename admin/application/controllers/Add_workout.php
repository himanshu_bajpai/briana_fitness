<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_workout extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		if(isset($_POST['submit_btn'])){
			$create_workout = $wd_ci_model->create_workout($_POST);
			header('Location:' . base_url() . 'index.php/workout_template');
		}

		$head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
        $exercises = $wd_ci_model->get_exercises();
        $get_categories = $wd_ci_model->get_categories();
		$this->load->view('add_workout',['head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer, 'exercises' => $exercises, 'categories' => $get_categories]);
	}

	function get_exercise_data(){
        $wd_ci_model = new WD_CI_Model();
        $exercise_id = $this->input->post();
        $exercise_data = $wd_ci_model->get_exercise_data($exercise_id['exercise_id']);
        if($exercise_data){
        	$reps = '';
        	$sets = '';
        	$rest = '';
        	for($i=1; $i<=10; $i++){
        		$reps.= "<option ".($exercise_data->reps == $i?'selected':'')." value='".$i."'>".$i."</option>";
        		if($i <= 5)
        			$sets.= "<option ".($exercise_data->sets == $i?'selected':'')." value='".$i."'>".$i."</option>";
        	}
        	for($j=30; $j<=60; $j = $j+15){
        		$rest.= "<option ".($exercise_data->rest == $j.'s'?'selected':'')." value='".$j."s'>".$j."s</option>";
        	}
        	$final_data = [
        		'reps' => $reps,
        		'sets' => $sets,
        		'rest' => $rest,
                'video_path' => $exercise_data->exercise_video,
                'image_path' => $exercise_data->exercise_image
        	];
            echo json_encode($final_data);
        }
    }
}