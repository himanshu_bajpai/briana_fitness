<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		if(isset($_POST['update'])){
			if($_FILES['category_image']['name']){
				$wd_ci_model->make_needed_directories('./uploads/categories');
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = TRUE;
                $config['file_name'] = date('U') . '_' . $_FILES['category_image']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('category_image')){
                    $data['category_id'] = $_POST['category_id'];
                    $data['category_name'] = $_POST['category_name'];
                    $data['category_image'] = $this->upload->data()["file_name"];
                    $data['key'] = 'update';
                    $wd_ci_model->update_category($data);
                }
			}else{
				$wd_ci_model->update_category($_POST);
			}
		}
		if($_GET){
			$wd_ci_model->delete_category($_GET['id']);
			header('Location:' . base_url() . 'index.php/categories');
		}
		$get_categories_with_sub_categories_count = $wd_ci_model->get_categories_with_sub_categories_count();
		$head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
		$this->load->view('categories',['categories' => $get_categories_with_sub_categories_count,'head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}
}