<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_subscription_packs extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
        $wd_ci_model = new WD_CI_Model();
        if($_POST){
            $res = $wd_ci_model->add_subscription_pack($_POST);
            header('Location:' . base_url() . 'index.php/subscription_packs');
        }
        $head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
        $this->load->view('add_subscription_packs',['head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}
}