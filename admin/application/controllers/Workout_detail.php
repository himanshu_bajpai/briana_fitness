<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Workout_detail extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		$id = $_GET['id'];
		$get_workout_details = $wd_ci_model->get_workout_details($id);
		$get_workout_section_data = $wd_ci_model->get_workout_section_data($id);
		$head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
		$this->load->view('workout_detail',['head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer, 'workout_detail' => $get_workout_details, 'section_data' => $get_workout_section_data]);
	}
}