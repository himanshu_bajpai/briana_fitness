<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fitness_levels extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('WD_CI_Model');
   	}

	public function index(){
		$wd_ci_model = new WD_CI_Model();
		if(isset($_POST['add'])){
			$wd_ci_model->add_fitness_level($_POST['name']);
		}
		if(isset($_POST['update'])){
			$wd_ci_model->update_fitness_level($_POST);
		}
		if($_GET){
			$wd_ci_model->delete_fitness_level($_GET['id']);
			header('Location:' . base_url() . 'index.php/fitness_levels');
		}
		$fitness_levels = $wd_ci_model->get_fitness_levels();
		$head = $this->load->view('head',[],true);
        $headers = $this->load->view('headers',[],true);
        $sidebar = $this->load->view('sidebar',[],true);
        $footer = $this->load->view('footer',[],true);
		$this->load->view('fitness_levels',['fitness_levels' => $fitness_levels,'head' => $head, 'headers' => $headers, 'sidebar' => $sidebar, 'footer' => $footer]);
	}
}