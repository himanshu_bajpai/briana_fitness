<footer class="footer">
	<div class="container-fluid">
		<div class="copyright ml-auto">2019 | Briana Fitness</div>				
	</div>
</footer>
<script src="<?php echo base_url() .'assets/js/jquery.3.2.1.min.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/popper.min.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/bootstrap.min.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/jquery-ui.min.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/jquery.ui.touch-punch.min.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/jquery.scrollbar.min.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/datatables.min.js'?>"></script>
<script src="<?php echo base_url() .'assets/js/atlantis.min.js'?>"></script>