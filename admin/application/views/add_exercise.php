<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Briana Fitness - Add Exercise</title>
		<?php print_r($head); ?>
	</head>
	<body>
		<div class="wrapper">
			<?php print_r($headers); ?>
			<div class="sidebar sidebar-style-2">			
				<div class="sidebar-wrapper scrollbar scrollbar-inner">
					<div class="sidebar-content">
					<?php print_r($sidebar); ?>
					</div>
				</div>
			</div>
			<div class="main-panel">
				<div class="content">
					<div class="panel-header bg-primary-gradient">
						<div class="page-inner py-5">
							<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
								<div>
									<h4 class="breadcumb text-white pb-2 fw-bold"><a href="exercises">Exercises</a>/Add Exercise</h4>
								</div>
							</div>
						</div>
					</div>
					<div class="page-inner mt--5">
						<div class="row">
	                        <div class="col-md-2"></div> 
							<div class="col-md-8">
								<div class="card">
									<div class="card-header">
	                                    <div class="row">
	                                        <div class="col-md-10"><h4 class="card-title">Add New Exercise</h4></div>    
	                                    </div>
									</div>
									<form action="" method="POST" enctype="multipart/form-data">
										<?php if($this->session->flashdata('error_exercise')){ ?>
                            			<div class="alert alert-danger alert-dismissable">
                                			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                			<?php echo $this->session->flashdata('error_exercise'); ?>
                            			</div>
                            			<?php } ?>
										<div class="card-body">
		                                    <div class="row">
		                                        <div class="col-sm-6">
		                                            <div class="row">
		                                              	<div class="col-sm-12">
		                                                    <div class="form-group">
		                                                        <label for="email2">Choose Category</label>
		                                                        <select class="form-control" id="category_id" name="category_id" required>
		                                                            	<option value="">--Select--</option>
		                                                           	<?php
																	if($categories){
									                                    foreach($categories as $row){?>
									                                    <option value="<?php echo $row->category_id; ?>"><?php echo $row->category_name; ?></option>
									                                    <?php }} ?>
		                                                        </select>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-12">
		                                                    <div class="form-group">
		                                                        <label for="email2">Choose Sub Category</label>
		                                                        <select class="form-control" id="sub_category_id" name="sub_category_id" required>
		                                                            <option value="">--Select--</option>
		                                                        </select>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    	<div class="col-sm-6">
													<div class="row">	      
    													<div class="col-md-6">
    														<center>
   																<div class="image-upload-prw" style=" text-align: center;"> 
		                                        					<img id="vphoto2" src="<?php echo base_url() .'assets/img/empty.png' ?>">
		                                           				</div><br>
		                                       					<div class="vuploadphoto">
		                                       						<input type="file" name="exercise_image" class="upfbtn" id="imgInp2">
		                                           					<button type="button" id="uploadbtnvalue2" class="btn btn-primary btn-sm">Upload Image</button>
		                                            				<button type="button" id="deletephoto2" class="btn btn-outline-danger btn-sm" style="display: none;">Remove</button>
		                                            			</div>
    														</center>
    													</div>
    													<div class="col-md-6">
    														<center>
   																<div class="image-upload-prw" style=" text-align: center;"> 
		                                        					<img id="vphoto" src="<?php echo base_url() .'assets/img/empty.png' ?>">
		                                           				</div><br>
		                                       					<div class="vuploadphoto"> 
		                                       						<input type="file" name="exercise_video" class="upfbtn" id="imgInp">
		                                           					<button type="button" id="uploadbtnvalue" class="btn btn-primary btn-sm">Upload Video</button>
		                                            				<button type="button" id="deletephoto" class="btn btn-outline-danger btn-sm" style="display: none;">Remove</button>
		                                            			</div>
    														</center>
    													</div>
    												</div>
		                                		</div>
		                                    <div class="col-sm-6">
		                                        <div class="form-group">
													<label for="email2">Name of Exercise</label>
													<input type="text" name="exercise_name" class="form-control" required>
										        </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                        <div class="form-group">
													<label for="email2">No. of Sets</label>
													<select class="form-control" name="sets" required>
		                                                <option value="">--Select--</option>
		                                                <option value="1">1</option>
		                                                <option value="2">2</option>
		                                                <option value="3">3</option>
		                                                <option value="4">4</option>
		                                                <option value="5">5</option>
		                                            </select>
										        </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                        <div class="form-group">
													<label for="email2">No. of Reps</label>
		                                            <select class="form-control" name="reps" required>
                                                        <option value="">--Select--</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    	<option value="6">6</option>
                                                    	<option value="7">7</option>
                                                    	<option value="8">8</option>
                                                    	<option value="9">9</option>
                                                    	<option value="10">10</option>
		                                            </select>
										        </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                        <div class="form-group">
													<label for="email2">Rest</label>
	                                                <select class="form-control" name="rest" required>
	                                                    <option value="30s">30 sec</option>
	                                                    <option value="45s">45 sec</option>
	                                                    <option value="60s">60 sec</option>
	                                                </select>
									            </div>
	                                        </div>
                                           	<div class="col-sm-12">
	                                            <div class="form-group">
													<label for="">Description</label>
													<textarea name="exercise_description" class="form-control" rows="4">About Exercise</textarea>
									            </div>
                                        	</div>
                                          	<div class="col-sm-12"><br>
	                                          	<button class="btn btn-primary btn-round">Add Exercise</button>
		                                        <button type="reset" class="btn btn-default btn-round">Cancel</button>
		                                    </div>
		                                </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php print_r($footer); ?>
		</div>
	</div>
	    <script type="text/javascript">
	    	$(document).ready(function(){
	    		$("#category_id").change(function(event){
      				event && event.preventDefault();
      				var category_id = $(this).val();
      				$.ajax({
        				url: 'Add_exercise/get_sub_categories_based_on_category',
        				data: {'category_id': category_id},
        				type: "post",
        				success: function(data){
        					$("#sub_category_id").html('');
        					$("#sub_category_id").append(data);
        				}
      				});
   				});

	        	$('#deletephoto').hide();
	        	$('#deletephoto2').hide();
	           	function readURL(input){
	  				if(input.files && input.files[0]){
	    				var reader = new FileReader();
	    				reader.onload = function(e){
	      					$('#vphoto').attr('src', e.target.result);
	    				}
	    				reader.readAsDataURL(input.files[0]);
	  				}
				}

				function readURL2(input){
	  				if(input.files && input.files[0]){
	    				var reader = new FileReader();
	    				reader.onload = function(e){
	      					$('#vphoto2').attr('src', e.target.result);
	    				}
	    				reader.readAsDataURL(input.files[0]);
	  				}
				}

				$("#imgInp").change(function(){
	  				readURL(this);
	    			$('#uploadbtnvalue').text("Change Video");
	     			$('#deletephoto').show().css({"position":"relative","z-index":"999"});
				});

				$("#imgInp2").change(function(){
	  				readURL2(this);
	    			$('#uploadbtnvalue2').text("Change Photo");
	     			$('#deletephoto2').show().css({"position":"relative","z-index":"999"});
				});
	        });
	        
	     	$('#deletephoto').on("click",function(){
	        	$(this).hide();
	          	$('#uploadbtnvalue').text("Upload Video");
	         	$('#vphoto').attr('src',"<?php echo base_url() . 'assets/img/empty.png'?>");
	     	});

	     	$('#deletephoto2').on("click",function(){
	        	$(this).hide();
	          	$('#uploadbtnvalue2').text("Upload Thumbnail");
	         	$('#vphoto2').attr('src',"<?php echo base_url() . 'assets/img/empty.png'?>");
	     	});
	    
	    	$("#allcheckbox").click(function(){
	     		$('input:checkbox').not(this).prop('checked', this.checked);
	        	$('#selecttxt').text(" Select All");
	        	if($('input#allcheckbox').is(':checked')){
	            	$('#selecttxt').text(" Unselect All");
	        	}
	 		});
	    </script>
	</body>
</html>