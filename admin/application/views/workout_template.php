<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Briana Fitness - Workout Template</title>
        <?php print_r($head); ?>
    </head>
    <body>
        <div class="wrapper">
            <?php print_r($headers); ?>
            <div class="sidebar sidebar-style-2">           
                <div class="sidebar-wrapper scrollbar scrollbar-inner">
                    <div class="sidebar-content">
                        <?php print_r($sidebar); ?>
                    </div>
                </div>
            </div>
        <div class="main-panel">
            <div class="content">
                <div class="panel-header bg-primary-gradient">
                    <div class="page-inner py-5">
                        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                            <div><h2 class="text-white pb-2 fw-bold">Workout Template Lists</h2></div>
                        </div>
                    </div>
                </div>
                <div class="page-inner mt--5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-10">
                                           <h4 class="card-title">Workout</h4>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="add_workout"><button class="btn btn-primary btn-round">New Workout</button></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="basic-datatables" class="display table table-hover" >
                                            <thead>
                                                <tr>
                                                    <th>Workout Name</th>
                                                    <th>Workout Category</th>  
                                                    <th>Workout Description</th>
                                                </tr>
                                            </thead>                                            
                                            <tbody>
                                                <?php
                                                    if($workout_list){
                                                        foreach($workout_list as $row){ ?>
                                                        <tr>
                                                            <td><a href="workout_detail?id=<?php echo $row->workout_id; ?>"><?php echo $row->workout_name; ?></a></td>
                                                            <td><?php echo $row->category_name; ?></td>
                                                            <td><?php echo $row->workout_description; ?></td>
                                                        </tr>
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php print_r($footer); ?>
        </div>
    </div>
        <script>
            $(document).ready( function(){
                $('#basic-datatables').DataTable();
            });
        </script>
    </body>
</html>