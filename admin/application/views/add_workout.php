<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Briana Fitness - New Workout</title>
        <?php print_r($head); ?>
    </head>
    <body>
        <div class="wrapper">
            <?php print_r($headers); ?>
            <div class="sidebar sidebar-style-2">           
                <div class="sidebar-wrapper scrollbar scrollbar-inner">
                    <div class="sidebar-content">
                        <?php print_r($sidebar); ?>
                    </div>
                </div>
            </div>
            <div class="main-panel">
                <div class="content">
                    <div class="panel-header bg-primary-gradient">
                        <div class="page-inner py-5">
                            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                <div><h2 class="text-white pb-2 fw-bold">New Workout Template</h2></div>
                            </div>
                        </div>
                    </div>
                    <div class="page-inner mt--5">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-10"><h4 class="card-title">Fill required information for new workout</h4></div>
                                        </div>
                                    </div>
                                    <form method="POST">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12 mt-20 accordntitle"><b>Workout Details</b></div>
                                                <div class="col-md-6 mt-20">
                                                    <label>Workout Name</label>
                                                    <input type="text" class="form-control" name="workout_name" required>
                                                </div>
                                                <div class="col-md-6 mt-20">
                                                    <label>Workout Category</label>
                                                    <select class="form-control" name="workout_category" required>
                                                        <option value="">--Select--</option>
                                                        <?php
                                                        if($categories){
                                                            foreach($categories as $row){?>
                                                        <option value="<?php echo $row->category_id; ?>"><?php echo $row->category_name; ?></option>
                                                        <?php }} ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 mt-20">
                                                    <label>Description</label>
                                                    <textarea class="form-control" rows="4" name="workout_description" required></textarea>
                                                </div>
                                            </div>
                                            <div class="row mt-20">
                                                <div class="col-md-12 accordntitle">
                                                    <b>Exercise Details</b>
                                                    <button id="add_sec" class="btn btn-primary btn-round pull-right" type="button" data-toggle="modal" data-target="#sectionmodel">Add Section</button>     
                                                </div>
                                                <div class="col-md-12 mt-20 toggle"></div>
                                            </div>
                                            <input style="margin: 10px 0 10px 0;" type="submit" name="submit_btn" value="Save" class="btn btn-primary btn-round pull-right" id="submit_form"/>
                                        </div>
                                    </form>
                                    <div class="modal" id="sectionmodel">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Add Section</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="text" name="sec_name[]" required id="sectionname" class="form-control" value="" placeholder="Please enter the section name...">
                                                    <span class="sec_err" style="color:red;"></span>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-warning" data-dismiss="modal" id="btn2">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php print_r($footer); ?>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#basic-datatables').DataTable();
                $("#btn2").click(function(){
                    if($('#sectionname').val() == ''){
                        $('.sec_err').html('Section cannot be empty');
                        return false;
                    }
                    var s_no = $('.sec_num').length;
                    $(".toggle").append('<div class="sectiondata mt-20"><div class="sectionheader"><input class="sec_num" value="'+ s_no  +'" type="hidden" name="sec_num[]" /> <input class="sec_name" value="'+ $('#sectionname').val()  +'" type="hidden" name="sec_name[]" /><p class="title">'+ $('#sectionname').val()  +'<span class="closed" style="float: right;cursor:pointer;"><i class="remove-section far fa-times-circle"></i></span></p></div> <div class="sectionbody"><button type="button" class="btn btn-primary btn-round" id="add-exercise">Add Exercise</button><div class="card1 p-10"></div></div></div>');
                });

                $(document).on("click", ".image-upload-prw" ,function(){
                    if($(this).find('#vphoto').attr('data-video') != '')
                        window.open($(this).find('#vphoto').attr('data-video'));
                })

                $("form").submit(function(e){
                    $('.alert').remove();
                    var sec_length = $('.toggle').find('.sectiondata').length;
                    if(sec_length > 0){
                        $(".sectiondata").each(function(){
                            if(!$(this).find('.exercisebox').length > 0){
                                e.preventDefault();
                                $("html, body").animate({ scrollTop: 0 }, "slow");
                                $('form').prepend('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Please select at least one exercise.</div>');
                            }
                        });
                    }else{
                        e.preventDefault();
                        $('form').prepend('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Please select at least one section.</div>');
                    }
                });

                $('#add_sec').click(function(){
                    $("#sectionname").val('');
                    $('.sec_err').html('');
                })

                $(document).on("click", "#add-exercise" ,function(){
                    var idk = $(this).parents('.sectiondata').find('.sec_num').val();
                    $(this).parents(".sectiondata").find(".card1").append('<div class="exercisebox"><div class="exercisebody"><div class="row"><div class="col-sm-12"><div class="form-group"><label>Exercise Name</label><span class="closed" style="float: right;cursor:pointer;"><i class="remove-exercise far fa-times-circle"></i></span><select class="form-control exercise_name" required name="ex_name[' + idk + '][]"><option value="">--Select--</option><?php if($exercises){ foreach($exercises as $row){ ?><option value="<?php echo $row->exercise_id.'~'.$row->exercise_name;?>"><?php echo $row->exercise_name; ?></option> <?php } } ?></select></div></div><div class="col-sm-4"><div class="form-group"><label for="email2">No.of Reps</label><select required class="form-control reps_name" name="reps_name[' + idk + '][]"><option value="">--Select--</option></select></div></div> <div class="col-sm-4"><div class="form-group"><label for="email2">No.of Sets</label><select required class="form-control sets_name" name="sets_name[' + idk + '][]"> <option value="">--Select--</option></select></div></div><div class="col-sm-4"><div class="form-group"><label for="email2">Rest</label><select class="form-control rest_name" name="rest_name[' + idk + '][]"><option>--Select--</option></select></div></div><div class="col-sm-12"><div class="form-group"><label>Notes</label><textarea class="form-control" rows="4" name="workout_notes[' + idk + '][]"></textarea></div></div><div class="col-sm-2"><div class="form-group"><label>Exercise Video</label><div class="image-upload-prw" style="text-align: center;position:relative;"><img id="vphoto" data-video="" src="<?php echo base_url() .'assets/img/empty.png';?>"><input class="hidden_video_url" type="hidden" value="" name = "video_path[' + idk + '][]" /><input class="hidden_image_url" type="hidden" value="" name = "image_path[' + idk + '][]" /></div></div></div></div></div></div></div>');
                });

                $(document).on("click", ".remove-exercise" ,function(){
                    $(this).parents(".exercisebox").remove();
                });

                $(document).on("click", ".remove-section" ,function(){
                    $(this).parents(".sectiondata").remove();
                });

                $(document).on("change", ".exercise_name" ,function(){
                    event && event.preventDefault();
                    var exercise_id = $(this).val();
                    var parent = $(this).parents(".exercisebox");
                    $.ajax({
                        url: 'Add_workout/get_exercise_data',
                        data: {'exercise_id': exercise_id},
                        type: "post",
                        dataType : "json",
                        success: function(data){
                            parent.find(".reps_name").html('');
                            parent.find(".reps_name").append(data.reps);
                            parent.find(".sets_name").html('');
                            parent.find(".sets_name").append(data.sets);
                            parent.find(".rest_name").html('');
                            parent.find(".rest_name").append(data.rest);
                            parent.find('#vphoto').prop('src', "<?php echo base_url().'uploads/exercises/images/' ?>" + data.image_path);
                            parent.find('#vphoto').attr('data-video', "<?php echo base_url().'uploads/exercises/videos/' ?>" + data.video_path);
                            parent.find('.hidden_video_url').val("<?php echo base_url().'uploads/exercises/videos/' ?>" + data.video_path);
                            parent.find('.hidden_image_url').val("<?php echo base_url().'uploads/exercises/images/' ?>" + data.image_path);
                        }
                    });
                });
            });
        </script>
    </body>
</html>