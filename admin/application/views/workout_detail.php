<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
        <title>Briana Fitness - Workout Details</title>
        <?php print_r($head); ?>
    </head>
	<body>
		<div class="wrapper">
			<?php print_r($headers); ?>
			<div class="sidebar sidebar-style-2">           
                <div class="sidebar-wrapper scrollbar scrollbar-inner">
                    <div class="sidebar-content">
                        <?php print_r($sidebar); ?>
                    </div>
                </div>
            </div>
			<div class="main-panel">
				<div class="content">
					<div class="panel-header bg-primary-gradient">
						<div class="page-inner py-5">
							<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
								<div><h2 class="text-white pb-2 fw-bold"> Workout Details</h2></div>
							</div>
						</div>
					</div>
					<div class="page-inner mt--5">
						<div class="row">
                        	<div class="col-md-2"></div>
							<div class="col-md-8">
								<div class="card">
									<div class="card-header">
                                    	<div class="row">
                                        	<div class="col-lg-12 ">
                                            	<div class="workoutdtitle"><?php echo $workout_detail->workout_name; ?></div>                                            
                                            	<div class="workoutddata">
                                                	<div class="row">
                                                    	<div class="col-md-2">
                                                        	<label>Video</label>
                                                        	<p ><i class="fa fa-play"></i> 32</p>
                                                    	</div>
                                                    	<div class="col-md-12">
	                                                        <label>About Workout</label>
	                                                        <p><?php echo $workout_detail->workout_description; ?></p>
                                                    	</div>
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
									</div>
									<div class="card-body">
                                    	<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
											<li class="nav-item submenu">
												<a class="nav-link active show" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Workout Section</a>
											</li>
										</ul>
                                    	<div class="tab-content mt-2 mb-3" id="pills-tabContent">
											<div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
							                	<div class="card-body sectp">
                                                	<div class="col-md-12 ">
                                                		<?php if($section_data){
                                                		foreach($section_data as $row){
                                                		?>
                                                		<div class="panel-group" id="accordion1">
                                                			<div class="panel panel-default">
                                                    			<div class="panel-heading">
                                                    				<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $row->section_id; ?>"><?php echo $row->section_name; ?></a></h4>
                                                    			</div>
                                                    			<div id="collapse<?php echo $row->section_id; ?>" class="panel-collapse collapse in">
                                                      				<div class="panel-body">
                                                        				<div class="row">
                                                        					<?php foreach($row->exercise_data as $row2) { ?>
                                                            				<div class="col-md-6 mt-20">
                                                                				<div class="store-cat-list" style="display:block;">
                                                                					<div class="d-flex" style="margin: 10px 0px;">
					                                                                    <div class="scat-image">
					                                                                        <img src="http://localhost/codeIgniter/assets/img/profile.jpg">
					                                                                    </div>
					                                                                    <div class="category-name">
					                                                                        <div class="scat-name_title"><?php echo $row2->workout_exercise_name; ?></div>
					                                                                        <div class="scat-name_details" title="">
					                                                                            <ul>
						                                                                            <li>Sets: <b><?php echo $row2->workout_exercise_sets; ?></b></li>
						                                                                            <li>Reps: <b><?php echo $row2->workout_exercise_reps; ?></b></li>
						                                                                            <li>Rest: <b><?php echo $row2->workout_exercise_rest; ?></b></li>
						                                                                        </ul>
					                                                                       	</div>
					                                                                        <div class="scat-icon"><div class="scat-icon__icon"></div></div>
					                                                                    </div>
					                                                                </div>
					                                                                <p class="para m-0"><strong>Notes:</strong><?php echo $row2->workout_notes; ?></p>
                                                                				</div>
                                                            				</div>
                                                            				<?php } ?>
                                                        				</div>
                                                      				</div>
                                                    			</div>
                                                  			</div>
                                                		</div>
                                                		<?php
                                                		}
                                                		}
                                                		?> 
                                                	</div>
                                            	</div>     
											</div>
											<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
												<div class="card-body">
	                                                <div class="col-md-12 mt-20  sectp notebox">
	                                                    <h4>Note1</h4>
	                                                    <p>Start by hanging from the bar, hands at approximately shoulder-width apart, palms pointing away from you, and elbows fully locked.</p>
	                                                </div>
                                                 	<div class="col-md-12 mt-20  sectp notebox">
	                                                    <h4>Note1</h4>
	                                                    <p>Start by hanging from the bar, hands at approximately shoulder-width apart, palms pointing away from you, and elbows fully locked.</p>
                                                	</div>
                                            	</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php print_r($footer); ?>
			</div>
		</div>
    	<script>
            $(document).ready( function(){
                $('#basic-datatables').DataTable();
            });
        </script>
	</body>
</html>