<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Briana Fitness - Exercises</title>
		<?php print_r($head); ?>
	</head>
	<body>
		<div class="wrapper">
			<?php print_r($headers); ?>
			<div class="sidebar sidebar-style-2">			
				<div class="sidebar-wrapper scrollbar scrollbar-inner">
					<div class="sidebar-content">
					<?php print_r($sidebar); ?>
					</div>
				</div>
			</div>

			<div class="main-panel">
				<div class="content">
					<div class="panel-header bg-primary-gradient">
						<div class="page-inner py-5">
							<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
								<div>
									<h2 class="text-white pb-2 fw-bold">Exercise Lists</h2>
								</div>
							</div>
						</div>
					</div>
					<div class="page-inner mt--5">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
	                                    <div class="row">
	                                        <div class="col-md-10">
	                                           <h4 class="card-title">Exercise table</h4>
	                                        </div>
	                                         <div class="col-md-2">
	                                            <a href="add_exercise"><button class="btn btn-primary btn-round">New Exercise</button></a>
	                                         </div>
	                                    </div>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="basic-datatables" class="display table table-hover" >
												<thead>
													<tr>
	                                                    <th>Image</th>
														<th>Name</th>
	                                                    <th style="width: 25%">Description</th>
	                                                    <th>Category</th>
	                                                    <th>Sub Category</th>
														<th>Sets</th>
														<th>Reps</th>
														<th>Rest</th>													
													</tr>
												</thead>											
												<tbody>
													<?php
													if($exercises){
				                                        foreach($exercises as $row){ ?>
				                                        <tr>
															<td><img style="height:47px; width:47px;" src="<?php echo base_url() . 'uploads/exercises/images/'. $row->exercise_image; ?>"></td>
															<td><?php echo $row->exercise_name; ?></td>
															<td><?php echo $row->exercise_description; ?></td>
															<td><?php echo $row->category_name; ?></td>
															<td><?php echo $row->sub_category_name; ?></td>
															<td><?php echo $row->sets; ?></td>
															<td><?php echo $row->reps; ?></td>
															<td><?php echo $row->rest; ?></td>
														</tr>
				                                        <?php
				                                        }
				                                    }
			                                    	?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php print_r($footer); ?>
			</div>
		</div>
	    <script>
	    	$(document).ready(function(){
	    		$('#basic-datatables').DataTable();
			});
	    </script>
	</body>
</html>