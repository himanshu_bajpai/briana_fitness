<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Briana Fitness - Trainer Details</title>
        <?php print_r($head); ?>
    </head>
    <body>
        <div class="wrapper">
            <?php print_r($headers); ?>
            <div class="sidebar sidebar-style-2">           
                <div class="sidebar-wrapper scrollbar scrollbar-inner">
                    <div class="sidebar-content"><?php print_r($sidebar); ?></div>
                </div>
            </div>
            <div class="main-panel">
                <div class="content">
                    <div class="panel-header bg-primary-gradient">
                        <div class="page-inner py-5">
                            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                <div><h2 class="text-white pb-2 fw-bold"> Trainers Lists</h2></div>
                            </div>
                        </div>
                    </div>
                    <div class="page-inner mt--5">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card">
                                    <?php if($trainer_details){ ?>
                                    <div class="card-body">
                                        <div class="mx-auto d-block">
                                            <div class="detailcoverimg mx-auto">
                                                <img class="rounded-circle  d-block" src="<?php echo $trainer_details[0]->profile_image; ?>" alt="Card image cap">
                                            </div>
                                            <h2 class="text-sm-center mt-2 mb-1"><?php echo $trainer_details[0]->name; ?> (<?php echo $trainer_details[0]->gender; ?>)</h2>
                                            <p><center class="green">Subscribed</center></p>
                                        </div>
                                        <hr>
                                        <div class="addrees_book">
                                            <p class="addline_line"><i class="fa fa-map-marker" style="color: #DD5A0A"></i> <?php echo $trainer_details[0]->city_state;?></p> 
                                            <p class="email_line"><i class="fa fa-envelope" style="color: #DD5A0A"></i>   <?php echo $trainer_details[0]->email; ?></p>
                                            <p class="phone_line"><i class="fa fa-phone" style="color: #DD5A0A"> </i>     <?php echo $trainer_details[0]->phone_number; ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-lg-4 ">
                                                <div class="card text-white bg-flat-color-1">
                                                    <div class="card-body pb-0 no-lr-padd">                                    
                                                        <div class="col-lg-12 ">
                                                            <center>
                                                                <p class="text-light">Total Workouts</p>
                                                                <p class="count text-light font-30">20</p>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>                            
                                            </div>
                                            <div class="col-lg-4 ">
                                                <div class="card text-white bg-flat-color-3">
                                                    <div class="card-body pb-0 no-lr-padd">                                
                                                        <div class="col-lg-12 ">
                                                            <center>
                                                                <p class="text-light">Total Clients</p>
                                                                <?php if($get_trainer_based_clients){ ?>
                                                                <p class="count text-light font-30"><?php echo count($get_trainer_based_clients); ?></p>
                                                                <?php } else { ?>
                                                                <p class="count text-light font-30">0</p>
                                                                <?php } ?>    
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>                            
                                            </div>
                                            <div class="col-lg-4 ">
                                                <div class="card text-white bg-flat-color-4">
                                                    <div class="card-body pb-0 no-lr-padd">                                    
                                                        <div class="col-lg-12 ">
                                                            <center>
                                                                <p class="text-light">Subscription upto</p>
                                                                <p class="count text-light font-30">12 April 2019</p>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
                                            <li class="nav-item submenu">
                                                <a class="nav-link active show" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Workouts</a>
                                            </li>
                                            <li class="nav-item submenu">
                                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Users</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content mt-2 mb-3" id="pills-tabContent">
                                            <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                                <div class="table-responsive">
                                                    <table id="basic-datatables" class="display table table-hover" >
                                                        <thead>
                                                            <tr>
                                                                <th>Workout Name</th>
                                                                <th>Created by:</th>
                                                                <th>Created on</th>   
                                                                <th>No.of Notes</th>
                                                                <th>No.of Videos</th>
                                                                <th>Clients</th>
                                                            </tr>
                                                        </thead>                                            
                                                        <tbody>
                                                            <tr>
                                                                <td>Chest, Tricep Exercises</td>                   
                                                                <td>Admin</td>
                                                                <td>25 dec, 2018</td>
                                                                <td>1</td>
                                                                <td>4</td>
                                                                <td>6</td>                                                    
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>      
                                            </div>
                                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                <div class="table-responsive">
                                                    <table id="basic-datatables" class="display table table-hover" >
                                                        <thead>
                                                            <tr>
                                                                <th>Image</th>
                                                                <th>Name</th>                                                  
                                                                <th>Email Address</th>
                                                                <th>Gender</th>
                                                                <th>Phone</th>
                                                                <th>State</th> 
                                                            </tr>
                                                        </thead>                                            
                                                        <tbody>
                                                        <?php
                                                        if($get_trainer_based_clients){
                                                            foreach($get_trainer_based_clients as $row){ ?>
                                                            <tr>
                                                                <td><img src="<?php echo $row->profile_image;?>"  class="table-thumb"></td>
                                                                <td><?php echo $row->name;?></td>
                                                                <td><?php echo $row->email;?></td>
                                                                <td><?php echo $row->gender;?></td>
                                                                <td><?php echo $row->phone_number;?></td>
                                                                <td><?php echo $row->state;?></td>
                                                            </tr>
                                                            <?php } } else{ ?>
                                                                <tr class="odd">
                                                                    <td style="text-align:center;" valign="top" colspan="6" class="dataTables_empty">No client available</td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php print_r($footer); ?>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#basic-datatables').DataTable();
            });
        </script>
    </body>
</html>