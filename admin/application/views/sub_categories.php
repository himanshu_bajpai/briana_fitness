<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Briana Fitness - Sub Categories</title>
		<?php print_r($head); ?>
	</head>
	<body>
		<div class="wrapper">
			<?php print_r($headers); ?>
			<div class="sidebar sidebar-style-2">			
				<div class="sidebar-wrapper scrollbar scrollbar-inner">
					<div class="sidebar-content">
					<?php print_r($sidebar); ?>
					</div>
				</div>
			</div>

			<div class="main-panel">
				<div class="content">
					<div class="panel-header bg-primary-gradient">
						<div class="page-inner py-5">
							<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
								<div>
									<h2 class="text-white pb-2 fw-bold">Sub Categories Lists</h2>
								</div>
							</div>
						</div>
					</div>
					<div class="page-inner mt--5">
						<div class="row">
                        	<div class="col-md-2"></div>
							<div class="col-md-8">
								<div class="card">
									<div class="card-header">
	                                    <div class="row">
	                                        <div class="col-md-10">
	                                           <h4 class="card-title">Sub Categories table</h4>
	                                        </div>
	                                         <div class="col-md-2">
	                                            <a href="add_sub_category"><button class="btn btn-primary btn-round">New Sub Category</button></a>
	                                         </div>
	                                    </div>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="basic-datatables" class="display table table-hover" >
												<thead>
													<tr>
	                                                    <th>Image</th>
														<th>Sub Category Name</th>
														<th>Parent Category</th>
														<th>Action</th>
												</thead>											
												<tbody>
												<?php
												if($get_sub_categories){
			                                        foreach($get_sub_categories as $row){ ?>
			                                        <tr>
														<td><img style="height:47px; width:47px;" src="<?php echo base_url() . 'uploads/sub_categories/'. $row->sub_category_image; ?>"></td>
														<td><?php echo $row->sub_category_name; ?></td>
														<td><?php echo $row->category_name; ?></td>
														<td>
                                                        	<a href="" data-toggle="modal" data-target="#edit_sub_category" onclick='myFunction(<?php echo json_encode($row); ?>);'>Edit</a> |
                                                        	<a href="#" onclick='myFunction2(<?php echo json_encode($row); ?>);'>Delete</a>
                                                    	</td>
													</tr>
			                                        <?php
			                                        }
			                                    }
			                                    ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php print_r($footer); ?>
			</div>
		</div>
		<div class="modal" id="edit_sub_category">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<h4 class="modal-title">Update Sub Category</h4>
        				<button type="button" class="close" data-dismiss="modal">&times;</button>
     				</div>
      				<div class="modal-body">
           				<form action="" method="POST" enctype="multipart/form-data">
           					<div class="col-sm-12">
                				<div class="form-group">
                    				<label for="email2">Sub Category Name</label>
                    				<input type="text" class="form-control edit_label" name="sub_category_name">
                    				<label for="email2">Sub Category Image</label><br/>
                    				<input type="file" name="sub_category_image">
                    				<input type="hidden" class="form-control edit_id" name="sub_category_id">
                				</div>
          					</div>
           					<div class="col-sm-12"><br>
               					<center>
               						<input type="submit" class="btn btn-primary btn-round" name="update" value="Update">
                   					<button type="reset" class="btn btn-default btn-round" data-dismiss="modal">Cancel</button>
                   				</center>
          					</div>
          				</form>
      				</div>
    			</div>
  			</div>
		</div>
	    <script>
    		$(document).ready(function(){
    			$('#basic-datatables').DataTable();
			});
			function myFunction(data){
    			$('.edit_label').val(data.sub_category_name)
    			$('.edit_id').val(data.sub_category_id)
    		}
    		function myFunction2(data){
    			if(confirm("Are you sure you want to delete!")){
    				window.location.href = "<?php echo base_url() . 'index.php/sub_categories?id='?>"+data.sub_category_id+"";
  				}
    		}
    	</script>
	</body>
</html>
