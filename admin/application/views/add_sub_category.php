<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Briana Fitness - Add Sub Category</title>
		<?php print_r($head); ?>
	</head>
	<body>
		<div class="wrapper">
			<?php print_r($headers); ?>
			<div class="sidebar sidebar-style-2">			
				<div class="sidebar-wrapper scrollbar scrollbar-inner">
					<div class="sidebar-content">
					<?php print_r($sidebar); ?>
					</div>
				</div>
			</div>
			<div class="main-panel">
				<div class="content">
					<div class="panel-header bg-primary-gradient">
						<div class="page-inner py-5">
							<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
								<div>
									<h4 class="breadcumb text-white pb-2 fw-bold"><a href="sub_categories">Sub Categories</a>/Add Sub Category</h4>
								</div>
							</div>
						</div>
					</div>
				<div class="page-inner mt--5">
					<div class="row">
                        <div class="col-md-3"></div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-header">
	                                    <div class="row">
	                                        <div class="col-md-10">
	                                           <h4 class="card-title">Add New Sub Category</h4>
	                                        </div>                                        
	                                    </div>
									</div>
									<form action="" method="POST" enctype="multipart/form-data">
										<?php if($this->session->flashdata('error_sub_category')){ ?>
                            			<div class="alert alert-danger alert-dismissable">
                                			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                			<?php echo $this->session->flashdata('error_sub_category'); ?>
                            			</div>
                            			<?php } ?>
										<div class="card-body">
		                                    <div class="row">
		                                    	<div class="col-sm-12">
				                                   	<center><div class="image-upload-prw" style=" text-align: center;"> 
				                                           <img id="vphoto" src="<?php echo base_url() .'assets/img/empty.png'?>">
				                                           </div><br>
					                                       <div class="vuploadphoto"><input type='file' name="sub_category_image" class="upfbtn" id="imgInp" />
					                                           <button type="button" id="uploadbtnvalue" class="btn btn-primary btn-sm">Upload Photo</button>
					                                            <button type="button" id="deletephoto" class="btn btn-outline-danger btn-sm">Remove</button>
					                                       </div>
					                                </center>
		                                		</div>
		                                        <div class="col-sm-6">
		                                            <div class="form-group">
														<label for="email2">Category</label>
		                                                <select class="form-control" name="category_id" required>
		                                                    <option value="">--Select--</option>
		                                                <?php
			                                            if($get_categories){
			                                                foreach($get_categories as $row){ ?>
			                                                <option value="<?php echo $row->category_id; ?>"><?php echo $row->category_name; ?></option>
			                                                <?php
			                                                 }
			                                            }
			                                            ?> 
		                                                </select>
										            </div>
		                                        </div>
		                                        <div class="col-sm-6">
		                                            <div class="form-group">
														<label for="email2">Name of Sub Category</label>
														<input type="text" required name="sub_category_name" class="form-control"  placeholder="">
										            </div>
		                                        </div>
		                                        <div class="col-sm-12"><br>
		                                           	<center><button  class="btn btn-primary btn-round">Add Sub Category</button>
		                                            <button type="reset" class="btn btn-default btn-round">Cancel</button></center>
		                                        </div>
	                                    	</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php print_r($footer); ?>
			</div>
		</div>
	   	<script type="text/javascript">
	    	$(document).ready(function(){
	        	$('#deletephoto').hide();
	           	function readURL(input){
	  				if(input.files && input.files[0]){
	    				var reader = new FileReader();
	    				reader.onload = function(e){
	      					$('#vphoto').attr('src', e.target.result);
	    				}
	    				reader.readAsDataURL(input.files[0]);
	  				}
				}
				$("#imgInp").change(function(){
	  				readURL(this);
	    			$('#uploadbtnvalue').text("Change Photo");
	     			$('#deletephoto').show().css({"position":"relative","z-index":"999"});
				});
	        });
	        
	     	$('#deletephoto').on("click", function(){
	         	$(this).hide();
	          	$('#uploadbtnvalue').text("Upload Photo");
	         	$('#vphoto').attr('src',"<?php echo base_url() .'assets/img/empty.png'?>");
	     	});
	    
	    	$("#allcheckbox").click(function(){
	     		$('input:checkbox').not(this).prop('checked', this.checked);
	        	$('#selecttxt').text(" Select All");
	        	if($('input#allcheckbox').is(':checked')){
	            	$('#selecttxt').text(" Unselect All");
	        	}
	 		});
	    </script>
	</body>
</html>