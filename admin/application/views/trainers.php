<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Briana Fitness - Trainers</title>
        <?php print_r($head); ?>
    </head>
    <body>
        <div class="wrapper">
            <?php print_r($headers); ?>
            <div class="sidebar sidebar-style-2">           
                <div class="sidebar-wrapper scrollbar scrollbar-inner">
                    <div class="sidebar-content">
                    <?php print_r($sidebar); ?>
                </div>
            </div>
        </div>
        <div class="main-panel">
            <div class="content">
                <div class="panel-header bg-primary-gradient">
                    <div class="page-inner py-5">
                        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                            <div>
                                <h2 class="text-white pb-2 fw-bold"> Trainers Lists</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-inner mt--5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-10">
                                           <h4 class="card-title"> Trainers table</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="basic-datatables" class="display table table-hover" >
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>                                                  
                                                    <th>Email Address</th>
                                                    <th>Gender</th>
                                                    <th>Phone</th>
                                                    <th>City</th>
                                                    <th>Subscription Status</th>        
                                                </tr>
                                            </thead>                                            
                                            <tbody>
                                                <?php
                                                if($get_trainers){
                                                    foreach($get_trainers as $row){ ?>
                                                    <tr>
                                                        <td><img style="height:47px; width:47px;" src="<?php echo $row->profile_image; ?>"></td>
                                                        <td><a href="trainer_details?id=<?php echo $row->id; ?>"><?php echo $row->name; ?></a></td>
                                                        <td><?php echo $row->email; ?></td>
                                                        <td><?php echo $row->gender; ?></td>
                                                        <td><?php echo $row->phone_number; ?></td>
                                                        <td><?php echo $row->city_state; ?></td>
                                                        <td>Subscribed</td>
                                                    </tr>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php print_r($footer); ?>
        </div>
    </div>
        <script>
            $(document).ready( function(){
                $('#basic-datatables').DataTable();
            });
        </script>
    </body>
</html>