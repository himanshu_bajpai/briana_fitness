<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/bootstrap.min.css'?>">
        <script src="<?php echo base_url() . 'assets/js/jquery.3.2.1.min.js'?>"></script>
        <script src="<?php echo base_url() . 'assets/js/popper.min.js'?>"></script>
        <script src="<?php echo base_url() . 'assets/js/bootstrap.min.js'?>"></script>
        <style>
            .klogin-cover { background: #fff;box-shadow: 0px 0px 20px rgba(0,0,0,0.2);padding: 45px;border-top: 4px solid #FEDC18;margin-top: 50%; }
        </style>
    </head>
    <body style="background:#0264b7">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>   
                <div class="col-md-6">
                    <div class="klogin-cover">
                        <div class="row">
                            <div class="col-md-12 pr-1">
                                <center><b>Admin Login</b></center>  
                            </div> 
                        </div>
                        <form action="" method="POST">

                            <?php if($this->session->flashdata('error_login')){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo $this->session->flashdata('error_login'); ?>
                            </div>
                            <?php } ?>

                            <div class="row">
                                <div class="col-md-12 pr-1" style="margin-bottom: 10px;"><br>
                                    <div class="form-group is-empty">
                                        <input name= "email" type="email" class="form-control" placeholder="name@domain.com" style="height: 45px;" required="required">
                                        <span class="material-input"></span> 
                                    </div>
                                </div> 
                                <div class="col-md-12 pr-1">
                                    <div class="form-group is-empty">
                                        <input type="password" name="password" class="form-control" required="required" placeholder="*********" style="height: 45px;">
                                        <span class="material-input"></span></div>
                                </div> 
                                <div class="col-md-12 pr-1"><br>
                                    <center><button type="submit" class="btn btn-primary" data-dismiss="modal" style="width: 70%;height: 50px;">Submit</button></center>
                                </div> 
                                <div class="col-md-12 pr-1"> <br>
                                    <center><label data-toggle="modal" data-target="#fpass">Forgot Password?</label></center>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>   
            </div>
        </div>
    </body>
</html>

