<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Briana Fitness - Fitness Levels</title>
		<?php print_r($head); ?>
	</head>
	<body>
		<div class="wrapper">
			<?php print_r($headers); ?>
			<div class="sidebar sidebar-style-2">			
				<div class="sidebar-wrapper scrollbar scrollbar-inner">
					<div class="sidebar-content">
					<?php print_r($sidebar); ?>
					</div>
				</div>
			</div>
			<div class="main-panel">
				<div class="content">
					<div class="panel-header bg-primary-gradient">
						<div class="page-inner py-5">
							<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
								<div>
									<h2 class="text-white pb-2 fw-bold">Fitness Levels</h2>
								</div>
							</div>
						</div>
					</div>
					<div class="page-inner mt--5">
						<div class="row">
                        	<div class="col-md-2"></div>   
							<div class="col-md-8">
								<div class="card">
									<div class="card-header">
                                    	<div class="row">
                                        	<div class="col-md-10">
                                           		<h4 class="card-title">Fitness Level table</h4>
                                        	</div>
                                         		<div class="col-md-2">
                                          			<button class="btn btn-primary btn-round" data-toggle="modal" data-target="#myModal1">Add New Level</button>
                                        		</div>
                                    	</div>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="basic-datatables" class="display table table-hover" >
												<thead>
													<tr>
	                                                    <th>Level Id</th>
														<th>Level Name</th>
	                                                    <th>Action</th>
                                                	</tr>
												</thead>											
												<tbody>
												<?php
                                                if($fitness_levels){
                                                    foreach($fitness_levels as $row){ ?>
                                                    <tr>
                                                        <td><?php echo $row->id; ?></td>
                                                        <td><?php echo $row->name; ?></td>
                                                        <td>
                                                        	<a href="" data-toggle="modal" data-target="#myEmodal1" onclick='myFunction(<?php echo json_encode($row); ?>);'>Edit</a> |
                                                        	<a href="#" onclick='myFunction2(<?php echo json_encode($row); ?>);'>Delete</a>
                                                    	</td>
                                                    </tr>
                                                    <?php
                                                    }
                                                }
                                                ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php print_r($footer); ?>
			</div>
		</div>
		<div class="modal" id="myModal1">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<h4 class="modal-title">New Level</h4>
        				<button type="button" class="close" data-dismiss="modal">&times;</button>
      				</div>
      				<div class="modal-body">
           				<form action="" method="POST">
           					<div class="col-sm-12">
            					<div class="form-group">
                					<label for="email2">Level Name</label>
                					<input type="text" name="name" class="form-control"  placeholder="" required>
            					</div>
          					</div>
           					<div class="col-sm-12"><br>
               					<center>
               						<input type="submit" class="btn btn-primary btn-round" name="add" value="Add">
                   					<button type="reset" class="btn btn-default btn-round" data-dismiss="modal">Cancel</button>
                   				</center>
          					</div>
          				</form>
      				</div>
    			</div>
  			</div>
		</div>
		<div class="modal" id="myEmodal1">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<h4 class="modal-title">Update Level</h4>
        				<button type="button" class="close" data-dismiss="modal">&times;</button>
     				</div>
      				<div class="modal-body">
           				<form action="" method="POST">
           					<div class="col-sm-12">
                				<div class="form-group">
                    				<label for="email2">Level Name</label>
                    				<input type="text" class="form-control edit_label" name="name">
                    				<input type="hidden" class="form-control edit_id" name="id">
                				</div>
          					</div>
           					<div class="col-sm-12"><br>
               					<center>
               						<input type="submit" class="btn btn-primary btn-round" name="update" value="Update">
                   					<button type="reset" class="btn btn-default btn-round" data-dismiss="modal">Cancel</button>
                   				</center>
          					</div>
          				</form>
      				</div>
    			</div>
  			</div>
		</div>
	    <script>
	    	$(document).ready(function(){
	    		$('#basic-datatables').DataTable();
			});
			function myFunction(data){
    			$('.edit_label').val(data.name)
    			$('.edit_id').val(data.id)
    		}
    		function myFunction2(data){
    			if(confirm("Are you sure you want to delete!")){
    				window.location.href = "<?php echo base_url() . 'index.php/fitness_levels?id='?>"+data.id+"";
  				}
    		}
	    </script>
	</body>
</html>