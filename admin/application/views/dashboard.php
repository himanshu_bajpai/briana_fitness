<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Briana Fitness - Dashboard</title>
		<?php print_r($head); ?>
	</head>
	<body>
		<div class="wrapper">
			<?php print_r($headers); ?>
			<div class="sidebar sidebar-style-2">			
				<div class="sidebar-wrapper scrollbar scrollbar-inner">
					<div class="sidebar-content">
					<?php print_r($sidebar); ?>
					</div>
				</div>
			</div>
			<div class="main-panel">
				<div class="content">
					<div class="panel-header bg-primary-gradient">
						<div class="page-inner py-5">
							<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
								<div>
									<h2 class="text-white pb-2 fw-bold">Dashboard</h2>
								</div>
							</div>
						</div>
					</div>
					<div class="page-inner mt--5">
						<div class="row mt--2">
							<div class="col-md-6">
								<div class="card full-height">
									<div class="card-body">
										<div class="card-title">Overall statistics</div>									
										<div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
											<div class="px-2 pb-2 pb-md-0 text-center">
												<div id="circles-1"></div>
												<h6 class="fw-bold mt-3 mb-0">Workouts</h6>
											</div>
											<div class="px-2 pb-2 pb-md-0 text-center">
												<div id="circles-2"></div>
												<h6 class="fw-bold mt-3 mb-0">Trainers</h6>
											</div>
											<div class="px-2 pb-2 pb-md-0 text-center">
												<div id="circles-3"></div>
												<h6 class="fw-bold mt-3 mb-0">Client</h6>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card full-height">
									<div class="card-body">
										<div class="card-title">Supscription statistics</div>
										<div class="row py-3">
											<div class="col-md-4 d-flex flex-column justify-content-around">
												<div>
													<h6 class="fw-bold text-uppercase text-success op-8">Subscribed Trainers</h6>
													<h3 class="fw-bold">82</h3>
												</div>
												<div>
													<h6 class="fw-bold text-uppercase text-danger op-8">Amount</h6>
													<h3 class="fw-bold">$1,248</h3>
												</div>
											</div>
											<div class="col-md-8">
												<div id="chart-container">
													<canvas id="totalIncomeChart"></canvas>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<div class="card-head-row">
											<div class="card-title">Trainers and Clients Statistics</div>
										</div>
									</div>
									<div class="card-body">
										<div class="chart-container" style="min-height: 375px">
											<canvas id="statisticsChart"></canvas>
										</div>
										<div id="myChartLegend"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php print_r($footer); ?>
			</div>
		</div>
		<script src="<?php echo base_url() . 'assets/js/chart.min.js'?>"></script>
		<script src="<?php echo base_url() . 'assets/js/jquery.sparkline.min.js'?>"></script>
		<script src="<?php echo base_url() . 'assets/js/circles.min.js'?>"></script>
		<script src="<?php echo base_url() . 'assets/js/bootstrap-notify.min.js'?>"></script>
		<script src="<?php echo base_url() . 'assets/js/jquery.vmap.min.js'?>"></script>
		<script src="<?php echo base_url() . 'assets/js/jquery.vmap.world.js'?>"></script>
		<script src="<?php echo base_url() . 'assets/js/sweetalert.min.js'?>"></script>
		<script src="<?php echo base_url() . 'assets/js/dashboard_script.js'?>"></script>
		<script>
			Circles.create({
				id:'circles-1',radius:45,value:60,maxValue:100,width:7,text: 5,colors:['#f1f1f1', '#FF9E27'],duration:400,wrpClass:'circles-wrp',textClass:'circles-text',styleWrapper:true,styleText:true
			})

			Circles.create({
				id:'circles-2',radius:45,value:70,maxValue:100,width:7,text: <?php echo $trainers_count; ?>,colors:['#f1f1f1', '#2BB930'],duration:400,wrpClass:'circles-wrp',textClass:'circles-text',styleWrapper:true,styleText:true
			})

			Circles.create({
				id:'circles-3',radius:45,value:40,maxValue:100,width:7,text: <?php echo $clients_count; ?>,colors:['#f1f1f1', '#F25961'],duration:400,wrpClass:'circles-wrp',textClass:'circles-text',styleWrapper:true,styleText:true
			})

			var totalIncomeChart = document.getElementById('totalIncomeChart').getContext('2d');

			var mytotalIncomeChart = new Chart(totalIncomeChart, {
				type: 'bar',
				data: {
					labels: ["S", "M", "T", "W", "T", "F", "S", "S", "M", "T"],
					datasets : [{
						label: "Amount",
						backgroundColor: '#ff9e27',
						borderColor: 'rgb(23, 125, 255)',
						data: [6, 4, 9, 5, 4, 6, 4, 3, 8, 10],
					}],
				},
				options: {
					responsive: true,
					maintainAspectRatio: false,
					legend: {
						display: false,
					},
					scales: {
						yAxes: [{
							ticks: {
								display: false
							},
							gridLines : {
								drawBorder: false,
								display : false
							}
						}],
						xAxes : [ {
							gridLines : {
								drawBorder: false,
								display : false
							}
						}]
					},
				}
			});

			$('#lineChart').sparkline([105,103,123,100,95,105,115], {
				type: 'line',height: '70',width: '100%',lineWidth: '2',lineColor: '#ffa534',fillColor: 'rgba(255, 165, 52, .14)'
			});
	</script>
	</body>
</html>