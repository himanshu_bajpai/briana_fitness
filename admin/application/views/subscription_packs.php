<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Briana Fitness - Subscription Packs</title>
        <?php print_r($head); ?>
    </head>
    <body>
        <div class="wrapper">
            <?php print_r($headers); ?>
            <div class="sidebar sidebar-style-2">           
                <div class="sidebar-wrapper scrollbar scrollbar-inner">
                    <div class="sidebar-content">
                    <?php print_r($sidebar); ?>
                    </div>
                </div>
            </div>
            <div class="main-panel">
                <div class="content">
                    <div class="panel-header bg-primary-gradient">
                        <div class="page-inner py-5">
                            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                <div>
                                    <h2 class="text-white pb-2 fw-bold">Subscription Lists</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-inner mt--5">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-10">
                                               <h4 class="card-title">Subscription table</h4>
                                            </div>
                                             <div class="col-md-2">
                                                <a href="add_subscription_packs"><button class="btn btn-primary btn-round">Add New Pack</button></a>
                                             </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="basic-datatables" class="display table table-hover" >
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Price</th>
                                                        <th>Validity</th>
                                                </thead>                                            
                                                <tbody>
                                                <?php
                                                if($get_subscription_packs){
                                                    foreach($get_subscription_packs as $row){ ?>
                                                    <tr>
                                                        <td><?php echo $row->subscription_pack_name; ?></td>
                                                        <td><?php echo $row->subscription_pack_price; ?></td>
                                                        <td><?php echo $row->subscription_pack_validity; ?></td>
                                                    </tr>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php print_r($footer); ?>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#basic-datatables').DataTable();
            });
        </script>
    </body>
</html>