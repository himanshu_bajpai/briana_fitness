<ul class="nav nav-primary">
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/dashboard') !== false) { 
    echo "active"; } ?>">
        <a href="dashboard">
            <i class="fas fa-home"></i>
            <p>Dashboard</p>
        </a>                            
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/workout_template') !== false) { 
    echo "active"; } ?>">
        <a href="workout_template">
            <i class="fa fa-file"></i>
            <p>Workout Template</p>
        </a>                         
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/categories') !== false || strpos(strtolower($_SERVER['PATH_INFO']), '/add_category') !== false) { 
    echo "active"; } ?>">
        <a href="categories">
            <i class="fa fa-th"></i>
            <p>Categories</p>
        </a>                       
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/sub_categories') !== false || strpos(strtolower($_SERVER['PATH_INFO']), '/add_sub_category') !== false) { 
    echo "active"; } ?>">
        <a href="sub_categories">
            <i class="fa fa-th"></i>
            <p>Sub Categories</p>
        </a>                       
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/exercises') !== false || strpos(strtolower($_SERVER['PATH_INFO']), '/add_exercise') !== false) { 
    echo "active"; } ?>">
        <a href="exercises">
            <i class="fa fa-universal-access"></i>
            <p>Exercises</p>
        </a>                            
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/trainers') !== false || strpos(strtolower($_SERVER['PATH_INFO']), '/trainer_details') !== false) { 
    echo "active"; } ?>">
        <a href="trainers">
            <i class="fas fa-user"></i>
            <p>Trainers</p>
        </a>                         
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/clients') !== false) { 
    echo "active"; } ?>">
        <a href="clients">
            <i class="fas fa-users"></i>
            <p>Clients</p>
        </a>                          
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/fitness_levels') !== false) { 
    echo "active"; } ?>">
        <a href="fitness_levels">
            <i class="fas fa-signal"></i>
            <p>Fitness Levels</p>
        </a>                          
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/subscription_packs') !== false || strpos(strtolower($_SERVER['PATH_INFO']), '/add_subscription_packs') !== false) { 
    echo "active"; } ?>">
        <a href="subscription_packs">
            <i class="fas fa-tags"></i>
            <p>Subscription Packs</p>
        </a>                           
    </li>
    <li class="nav-item <?php if(strpos(strtolower($_SERVER['PATH_INFO']), '/subscribed_trainers') !== false) { 
    echo "active"; } ?>">
        <a href="subscribed_trainers">
            <i class="fas fa-user"></i>
            <p>Subscribed Trainers</p>
        </a>                          
    </li>
</ul>
