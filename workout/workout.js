var con = require('../connection/connection');
var async = require('async');
var sgMail = require('@sendgrid/mail');

module.exports.get_workout_categories = function(req,res){
	con.query("SELECT * from categories",function(err,rows){
		if(err){
			res.send({
				"status" : 0,
				"message" : "Error while fetching categories"
			})
		}else{
			res.send({
				"status" : 1,
				"message" : "Categories data",
				"data" : rows
			})
		}
	})
}

module.exports.get_workout_based_on_category = function(req,res){
	con.query("SELECT workout_id,workout_name,workout_description from workout where workout_category = '" + req.body.category_id + "' ",function(err,rows){
		if(err){
			res.send({
				"status" :0,
				"message" :"Error while fetching workout"
			})
		}else{
			if(!rows.length){
				res.send({
					"status" :0,
					"message" : "No workout for category"
				})
			}else{
				res.send({
					"status" : 1,
					"message" : "Workout exist",
					"data" : rows
				})
			}
		}
	})
}

module.exports.get_workout_details = function(req,res){
	con.query("SELECT section_id,section_name from workout_sections where workout_id = '" + req.body.workout_id + "' ",function(err,rows){
		if(err){
			res.send({
				"status" :0,
				"message" :"Error while fetching workout sections"
			})
		}else{
			var final_data = [];
        	var i = 0;
        	async.mapSeries(rows, function(data,callback){
	          	con.query("SELECT workout_exercise_id, workout_exercise_name, workout_exercise_reps, workout_exercise_sets, workout_exercise_rest,workout_notes as workout_exercise_notes, workout_exercise_video, workout_exercise_image as workout_exercise_thumbnail from workout_exercises where section_id = '" + data.section_id + "' ",function(err,rows2){
	          		if(err){
	          			res.send({
	          				"status" : 0,
	          				"message" : "Error while fetching workout exercises"
	          			})
	          		}else{
	          			data.exercise_data = rows2;
            			final_data[i]  = data;
            			i++;
            			return callback(null, final_data);
	          		}
	          	})
        	},function(err,results){
          		res.send({
          			"status" :1,
          			"message" : "Workout data",
          			"data" : final_data
          		})
        	});
		}
	})
}

module.exports.get_exercises = function(req,res){
	con.query("SELECT exercise_id as workout_exercise_id,exercise_name as workout_exercise_name,sets as workout_exercise_sets,reps as workout_exercise_reps,rest as workout_exercise_rest, exercise_video as workout_exercise_video,exercise_image as workout_exercise_thumbnail from exercises",function(err,rows){
		if(err){
			res.send({
				"status" : 0,
				"message" : "Error while fetching exercises"
			})
		}else{
			if(!rows.length){
				res.send({
					"status" : 0,
					"message" : "No exercise"
				})
			}else{
				for(var i=0;i<rows.length; i++){
					rows[i].workout_exercise_notes = '';
				}
				res.send({
					"status" : 1,
					"message" : "Exercise data",
					"data" : rows
				})
			}
		}
	})
}

module.exports.create_workout = function(req,res){
	var parsed_data = JSON.parse(req.body.data);
	var current_date = Math.floor(new Date().valueOf() / 1000);
    con.query("INSERT into trainer_workout(workout_name,workout_description,workout_category,trainer_id,created_at) values('" + parsed_data.workout_name + "', '" + parsed_data.workout_description + "', '" + parsed_data.workout_category + "','" + parsed_data.trainer_id + "','" + current_date + "')",function(err,result){
    	if(err){
    		res.send({
    			"status":0,
    			"message":"Error while creating workout"
    		})
    	}else{
    		var i=0;
    		parsed_data.section_data.forEach(function(element){
    			con.query("INSERT into trainer_workout_sections(workout_id,section_name) values('" + result.insertId + "','" + element.section_name + "')",function(err,result2){
    				if(err){
    					res.send({
    						"status" : 0,
    						"message": "Error while creating workout"
    					})
    				}else{
    					element.exercise_data.forEach(function(element2){
							con.query("INSERT into trainer_workout_exercises(section_id,workout_exercise_name,workout_exercise_reps,workout_exercise_sets,workout_exercise_rest,workout_notes,workout_exercise_video,workout_exercise_image) values('" + result2.insertId + "','" + element2.workout_exercise_name + "','" + element2.workout_exercise_reps + "','" + element2.workout_exercise_sets + "','" + element2.workout_exercise_rest + "','" + element2.workout_exercise_notes + "','" + element2.workout_exercise_video + "','" + element2.workout_exercise_thumbnail + "')",function(err,result3){
    							if(err){
    								res.send({
    									"status" : 0,
    									"message" : "Error while creating workout"
    								})
    							}else{
    								i++;
    								if(i == parsed_data.section_data.length){
    									res.send({
											"status" : 1,
											"message" : "Workout created",
											"workout_id" : result.insertId
										})
    								}
    							}
    						})
						});
    				}
    			})
			});
    	}
    })
}

module.exports.client_list_for_assigning_workout = function(req,res){
	con.query("Select client_trainer_relationship.user_id, users.name as user_name, users.profile_image as user_image, users.email as user_email from client_trainer_relationship inner join users on client_trainer_relationship.user_id = users.id where client_trainer_relationship.trainer_id = '" + req.body.trainer_id + "' and client_trainer_relationship.status = 'Accepted' ",function(err,rows){
		if(err){
			res.send({
				"status" : 0,
				"message" : "Error while fetching assigned client list"
			})
		}else{
			con.query("SELECT GROUP_CONCAT(client_id) as client_id from assigned_workouts where workout_id= '" + req.body.workout_id + "' ", function(err,rows2){
				if(err){
					res.send({
						"status" : 0,
						"message" : "Error while fetching assigned client list"
					})
				}else{
					if(rows2[0].client_id == null){
                		for(var i=0; i<rows.length; i++){
	                    	rows[i].status = 'Not Assigned';
	                    }
                	}else{
                		var client_id = rows2[0].client_id.split(',');
                    	for(var i=0; i<rows.length; i++){
								var index = client_id.indexOf(rows[i].user_id);
	                    	if(index > -1)
	                    		rows[i].status = 'Assigned';
	                    	else
	                    		rows[i].status = 'Not Assigned';
	                    }
                	}
                	res.send({
                		"status":1,
                		"message":"Assigned client list",
                		"data":rows
                	})
				}
			});
		}
	});
}

module.exports.assign_workout = function(req,res){
	var client_id = req.body.client_id.split(',');
    async.mapSeries(client_id,function(data,callback){
    	con.query("INSERT into assigned_workouts(workout_id,client_id,workout_date) values('" + req.body.workout_id + "','" + data + "','" + req.body.workout_date + "') ",function(err,result){
    		if(err){
    			res.send({
    				"status" : 0,
    				"message" : "Error while assigning workout"
    			})
    		}else{
    			con.query("SELECT name,email from users where id = '" + data + "' ",function(err,rows){
    				if(err){
    					res.send({
    						"status" : 0,
    						"message" : "Error while sending email"
    					})
    				}else{
    					process.env.SENDGRID_API_KEY = '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';
		        		sgMail.setApiKey(process.env.SENDGRID_API_KEY);

				        var msg = {
				            to: rows[0].email,
				            from: 'no-reply@briana-fitness.com',
				            templateId : 'd-c0290faf26314d21abb5d413be6ec5b2',
				            dynamic_template_data: {
				                client_name: rows[0].name,
				                trainer_name : req.body.trainer_name,
				                workout_name : req.body.workout_name
				            }
				        };
		        		sgMail.send(msg);
    				}
    			})
    			return callback(null, '');
    		}
    	})
    },function(err,results){
  		res.send({
  			"status" :1,
  			"message" : "Workout assigned",
  		})
	});
}

module.exports.get_trainer_workout = function(req,res){
	con.query("SELECT workout_id,workout_name,workout_description,workout_category,created_at from trainer_workout where trainer_id = '" + req.body.trainer_id + "' ",function(err,rows){
		if(err){
			res.send({
				"status" : 0,
				"message" : "Error while fetching trainer workout data"
			})
		}else{
			var final_data = [];
			var i=0;
			async.mapSeries(rows,function(data,callback){
				con.query("SELECT users.profile_image as user_image, users.name as user_name from assigned_workouts inner join users on assigned_workouts.client_id = users.id where assigned_workouts.workout_id = '" + data.workout_id + "' ",function(err,rows2){
					if(err){
						res.send({
							"status" :0,
							"message" : "Error while fetching trainer workout data"
						})
					}else{
						con.query("SELECT count(trainer_workout_exercises.workout_exercise_video) as count from trainer_workout_sections inner join trainer_workout_exercises on trainer_workout_sections.section_id = trainer_workout_exercises.section_id where trainer_workout_sections.workout_id = '" + data.workout_id + "' and trainer_workout_exercises.workout_exercise_video != '' ",function(err,rows3){
							if(err){
								res.send({
									"status" : 0,
									"message" : "Error while fetching trainer workout data"
								})
							}else{
								con.query("SELECT count(trainer_workout_exercises.workout_notes) as count from trainer_workout_sections inner join trainer_workout_exercises on trainer_workout_sections.section_id = trainer_workout_exercises.section_id where trainer_workout_sections.workout_id = '" + data.workout_id + "' and trainer_workout_exercises.workout_notes != '' ",function(err,rows4){
									if(err){
										res.send({
											"status" : 0,
											"message" : "Error while fetching trainer workout data"
										})
									}else{
										con.query("SELECT count(trainer_workout_exercises.workout_exercise_id) as count from trainer_workout_sections inner join trainer_workout_exercises on trainer_workout_sections.section_id = trainer_workout_exercises.section_id where trainer_workout_sections.workout_id = '" + data.workout_id + "' ",function(err,rows5){
											if(err){
												res.send({
													"status" : 0,
													"message" : "Error while fetching trainer workout data"
												})
											}else{
												data.client_data = rows2;
												data.video_count = rows3[0].count;
												data.notes_count = rows4[0].count;
												data.exercise_count = rows5[0].count;
												final_data[i] = data;
												i++;
												return callback(null, final_data);
											}
										})
									}
								})
							}
						})
					}
				})
			},function(err,results){
				if(final_data.length > 0){
					res.send({
			  			"status" :1,
			  			"message" : "Workout data",
			  			"data" : final_data
			  		})
				}else{
					res.send({
			  			"status" :0,
			  			"message" : "No Workout data",
			  			"data" : final_data
			  		})
				}
			});
		}
	})
}

module.exports.get_trainer_workout_details = function(req,res){
	con.query("SELECT section_id,section_name from trainer_workout_sections where workout_id = '" + req.body.workout_id + "' ",function(err,rows){
		if(err){
			res.send({
				"status" :0,
				"message" :"Error while fetching workout sections"
			})
		}else{
			var final_data = [];
        	var i = 0;
        	async.mapSeries(rows, function(data,callback){
	          	con.query("SELECT workout_exercise_id, workout_exercise_name, workout_exercise_reps, workout_exercise_sets, workout_exercise_rest,workout_notes as workout_exercise_notes, workout_exercise_video, workout_exercise_image as workout_exercise_thumbnail from trainer_workout_exercises where section_id = '" + data.section_id + "' ",function(err,rows2){
	          		if(err){
	          			res.send({
	          				"status" : 0,
	          				"message" : "Error while fetching workout exercises"
	          			})
	          		}else{
	          			data.exercise_data = rows2;
    					final_data[i]  = data;
    					i++;
    					return callback(null, final_data);
	          		}
	          	})
        	},function(err,results){
        		con.query("SELECT count(trainer_workout_exercises.workout_exercise_id) as count from trainer_workout_sections inner join trainer_workout_exercises on trainer_workout_sections.section_id = trainer_workout_exercises.section_id where trainer_workout_sections.workout_id = '" + req.body.workout_id + "' ",function(err,rows3){
					if(err){
						res.send({
							"status" : 0,
							"message" : "Error while fetching trainer workout data"
						})
					}else{
						res.send({
		          			"status" :1,
		          			"message" : "Workout data",
		          			"data" : final_data,
		          			"exercise_count" : rows3[0].count
		          		})
					}
				})
        	});
		}
	})
}

module.exports.get_client_workout = function(req,res){
	con.query("SELECT assigned_workouts.workout_date, trainer_workout.workout_id, trainer_workout.workout_name, trainer_workout.workout_description from assigned_workouts inner join trainer_workout on assigned_workouts.workout_id = trainer_workout.workout_id where assigned_workouts.client_id = '" + req.body.client_id + "' ",function(err,rows){
		if(err){
			res.send({
				"status" : 0,
				"message" : "Error while fetching client workout"
			})
		}else{
			res.send({
				"status" :1,
				"message" : "Client workout data",
				"data" : rows
			})
		}
	})
}

module.exports.get_today_workout = function(req,res){
	con.query("SELECT trainer_workout.workout_id,trainer_workout.workout_name,trainer_workout.workout_description from assigned_workouts inner join trainer_workout on assigned_workouts.workout_id = trainer_workout.workout_id where assigned_workouts.client_id = '" + req.body.client_id + "' and assigned_workouts.workout_date = '" + req.body.workout_date + "' ",function(err,rows){
		if(err){
			res.send({
				"status" : 0,
				"message" : "Error while fetching client today workout data"
			})
		}else{
			con.query("SELECT section_id,section_name from trainer_workout_sections where workout_id= '" + rows[0].workout_id + "' ",function(err,rows2){
				if(err){
					res.send({
						"status" :0,
						"message":"Error while fetching client today workout data"
					})
				}else{
					rows[0].section_data = rows2;
					var final_data = [];
        			var i = 0;
        			async.mapSeries(rows2,function(data,callback){
        				con.query("SELECT workout_exercise_id, workout_exercise_name, workout_exercise_reps, workout_exercise_sets, workout_exercise_rest,workout_notes as workout_exercise_notes, workout_exercise_video, workout_exercise_image as workout_exercise_thumbnail from trainer_workout_exercises where section_id = '" + data.section_id + "' ",function(err,rows3){
			          		if(err){
			          			res.send({
			          				"status" : 0,
			          				"message" : "Error while fetching workout exercises"
			          			})
			          		}else{
			          			data.exercise_data = rows3;
		    					final_data[i]  = data;
		    					i++;
		    					return callback(null, final_data);
			          		}
			          	})
        			},function(err,results){
        				res.send({
							"status" : 1,
							"message" : "Client today workout data",
							"data" : final_data
						})
        			});
				}
			})
		}
	})
}

module.exports.edit_workout = function(req,res){
	var parsed_data = JSON.parse(req.body.data);
	con.query("UPDATE trainer_workout SET workout_name = '" + parsed_data.workout_name + "', workout_description = '" + parsed_data.workout_description + "' where workout_id = '" + req.body.workout_id + "' ",function(err,result){
		if(err){
			res.send({
				"status" :0,
				"message" : "Error while updating workout"
			})
		}else{
			con.query("DELETE trainer_workout_sections, trainer_workout_exercises FROM trainer_workout_sections, trainer_workout_exercises WHERE trainer_workout_sections.section_id = trainer_workout_exercises.section_id AND trainer_workout_sections.workout_id = '" + req.body.workout_id + "' ",function(err,result2){
				if(err){
					res.send({
						"status" :0,
						"message" : "Error while updating workout"
					})
				}else{
					var i=0;
		    		parsed_data.section_data.forEach(function(element){
		    			con.query("INSERT into trainer_workout_sections(workout_id,section_name) values('" + req.body.workout_id + "','" + element.section_name + "')",function(err,result3){
		    				if(err){
		    					res.send({
		    						"status" : 0,
		    						"message": "Error while creating workout"
		    					})
		    				}else{
		    					element.exercise_data.forEach(function(element2){
									con.query("INSERT into trainer_workout_exercises(section_id,workout_exercise_name,workout_exercise_reps,workout_exercise_sets,workout_exercise_rest,workout_notes,workout_exercise_video,workout_exercise_image) values('" + result3.insertId + "','" + element2.workout_exercise_name + "','" + element2.workout_exercise_reps + "','" + element2.workout_exercise_sets + "','" + element2.workout_exercise_rest + "','" + element2.workout_exercise_notes + "','" + element2.workout_exercise_video + "','" + element2.workout_exercise_thumbnail + "')",function(err,result3){
		    							if(err){
		    								res.send({
		    									"status" : 0,
		    									"message" : "Error while creating workout"
		    								})
		    							}else{
		    								if(i == parsed_data.section_data.length){
		    									res.send({
													"status" : 1,
													"message" : "Workout updated successfully"
												})
		    								}
		    								i++;
		    							}
		    						})
								});
		    				}
		    			})
					});
				}
			})
		}
	})
}
