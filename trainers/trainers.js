var con = require('../connection/connection');
var FCM = require('fcm-node');
var serverKey = 'AAAADJ64yO4:APA91bEd6FHBDYaORNh4FeUh81PSa8U_oQBTAaxV8A8r8Kv2v-HXaYcqOc8R_r2kAVgv1QkM5ZsPzhwj9rhohOgf3CeHbCed_TL9FzwwxM6b5ZeZdSpgXiHR25xbY2cgrX_DhnfLD9Na';
var fcm = new FCM(serverKey);
var async = require('async');

module.exports.request_status = function(req,res){
	con.query("SELECT name from users where id = '" + req.body.user_id + "' ",function(err,client_rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while fetching client data"
	        })
		}else{
			if(!client_rows.length){
				res.send({
		            "status":0,
		            "message":"Client doesnot exist"
		        })
			}else{
				con.query("UPDATE client_trainer_relationship SET status = '" + req.body.status + "' where user_id = '" + req.body.user_id + "' and trainer_id = '" + req.body.trainer_id + "' ",function(err, result){
					if(err){
						res.send({
			                "status":0,
			                "message":"There is an error while updating client-trainer relationship data"
			            })
					}else{
						con.query("SELECT name from trainers where id = '" + req.body.trainer_id + "' ",function(err,trainer_data){
							if(err){
								res.send({
					                "status":0,
					                "message":"There is an error while fetching trainer data"
					            })
							}else{
								con.query("SELECT device_token from users where id = '" + req.body.user_id + "' and notification = 'yes' ",function(err,client_data){
									if(err){
										res.send({
							                "status":0,
							                "message":"There is an error while fetching client data"
							            })
									}else{
										if(req.body.status == 'Accepted'){
											var title = 'Request Accepted';
											var body = 'Your request has been accepted by ' + trainer_data[0].name;
											var status = 1;
										}else{
											var title = 'Request Rejected';
											var body = 'Your request has been rejected by ' + trainer_data[0].name;
											var status = 1;
											con.query("DELETE from client_trainer_relationship where status = 'Rejected' ", function(err, result2){
												if(err){
													res.send({
										                "status":0,
										                "message":"There is an error while rejecting request"
										            })
												}
											})
										}
										var message = {
							    			to: client_data[0].device_token,
										    notification: {
										        title: title, 
										        body: body
										    },
										    data:{
										    	"notification_type" : "request_response"
										    }
										};
										fcm.send(message,function(err,response){
										    if(err)
										       	console.log(err);
										    else
										    	console.log('elseeeeeeee')     
										})
										res.send({
							                "status":status,
							                "message":body
							            })
									}
								})
							}
						})
					}
				})
			}
		}
	})
}

module.exports.get_client_list = function(req,res){
	if(req.body.group_id != ''){
		con.query("SELECT users.name, users.email, users.profile_image, users.state, users.country, client_trainer_relationship.user_id FROM users INNER JOIN client_trainer_relationship ON users.id = client_trainer_relationship.user_id where client_trainer_relationship.trainer_id = '" + req.body.trainer_id + "' and status = 'Accepted' group by client_trainer_relationship.user_id ",function(err,rows){
	        if(err){
				res.send({
	                "status":0,
	                "message":"There is an error while fetching clients data"
	            })
			}else{
				if(!rows.length){
		            res.send({
		                "status":0,
		                "message":"Clients does not exist"
		            });
		        }else{
		            con.query("SELECT GROUP_CONCAT(member_id) as member_id from group_members where group_id= '" + req.body.group_id + "' and is_removed= '0' ", function(err,rows2){
	                    if(err){
	                        res.send({
	                            "status":0,
	                            "message":"error ocurred while fetching group members data"
	                        })
	                    }else{
	                    	if(rows2[0].member_id == null){
	                    		for(var i=0; i<rows.length; i++){
			                    	rows[i].status = 'Not Added';
			                    }
	                    	}else{
	                    		var member_id = rows2[0].member_id.split(',');
		                    	for(var i=0; i<rows.length; i++){
	  								var index = member_id.indexOf(rows[i].user_id);
			                    	if(index > -1)
			                    		rows[i].status = 'Added';
			                    	else
			                    		rows[i].status = 'Not Added';
			                    }
	                    	}
	                    	con.query("SELECT GROUP_CONCAT(client_id) as client_id from group_requests where group_id = '" + req.body.group_id + "' ",function(err,rows3){
	                    		if(err){
	                    			res.send({
			                            "status":0,
			                            "message":"error ocurred while fetching group members data"
			                        })
	                    		}else{
	                    			if(rows3[0].client_id != null){
			                    		var member_id = rows3[0].client_id.split(',');
				                    	for(var j=0; j<rows.length; j++){
			  								var index = member_id.indexOf(rows[j].user_id);
					                    	if(index > -1)
					                    		rows[j].status = 'Requested';
					                    }
			                    	}
	                    		}
	                    		res.send({
				                    "status":1,
				                    "message":"Clients exist",
				                    "data" : rows
				                });
	                    	})
	                    }
		            })
		        }
			}
    	});
	}else{
		con.query("Select client_trainer_relationship.user_id, users.name, users.email, users.profile_image,users.country, users.state from client_trainer_relationship INNER JOIN users ON client_trainer_relationship.user_id = users.id where client_trainer_relationship.trainer_id = '" + req.body.trainer_id + "' and client_trainer_relationship.status = 'Accepted' group by client_trainer_relationship.user_id ",function(err,data){
			if(err){
				res.send({
	                "status":0,
	                "message":"There is an error while fetching client data"
	            })
			}else{
				if(!data.length){
					res.send({
			            "status":0,
			            "message":"Client doesnot exist"
			        })
				}else{
					res.send({
			            "status":1,
			            "message":"Client exist",
			            "data" : data
			        })
				}
			}
		})
	}
}

module.exports.get_requested_client_list = function(req,res){
	con.query("Select client_trainer_relationship.user_id, users.name, users.profile_image,users.country, users.state from client_trainer_relationship INNER JOIN users ON client_trainer_relationship.user_id = users.id where client_trainer_relationship.trainer_id = '" + req.body.trainer_id + "' and client_trainer_relationship.status = 'Requested' and client_trainer_relationship.sender_type != 'trainer' ",function(err,data){
		if(err){
			res.send({
                "status":0,
                "message":"There is an error while fetching client data"
            })
		}else{
			if(!data.length){
				res.send({
		            "status":0,
		            "message":"Client doesnot exist"
		        })
			}else{
				res.send({
		            "status":1,
		            "message":"Client exist",
		            "data" : data
		        })
			}
		}
	})
}

module.exports.delete_client = function(req,res){
	con.query("DELETE from client_trainer_relationship where user_id = '" + req.body.user_id + "' and trainer_id = '" + req.body.trainer_id + "' ",function(err,data){
		if(err){
			res.send({
                "status":0,
                "message":"There is an error while deleting client"
            })
		}else{
			res.send({
	            "status":1,
	            "message":"Client deleted successfully"
	        })
		}
	})
}

module.exports.get_all_clients = function(req,res){
	con.query("Select GROUP_CONCAT(user_id) as user_id from client_trainer_relationship where trainer_id = '" + req.body.trainer_id + "' and status = 'Accepted' ",function(err,rows){
        if(err){
			res.send({
                "status":0,
                "message":"There is an error while fetching client_trainer_relationship data"
            })
		}else{
			if(rows[0].user_id == null)
				var query = "SELECT id as user_id, name, email, profile_image from users";
			else
				var query = "SELECT id as user_id, name, email, profile_image from users where id NOT IN (" + rows[0].user_id + ") ";

			con.query(query,function(err, rows2){
        		if(err){
        			res.send({
		                "status":0,
		                "message":"There is an error while fetching client data"
		            })
        		}else{
        			var final_data = [];
            		var i = 0;
		            async.mapSeries(rows2, function(data,callback){
		                con.query("SELECT status, sender_id, sender_type from client_trainer_relationship where trainer_id = '" + req.body.trainer_id + "' and user_id = '" + data.user_id + "' ", function (err,rows2){
		                    if(err){
		                        res.send({
		                            "status":0,
		                            "message":"error ocurred while fetching data"
		                        })
		                    }
		                    if(rows2 != ''){
		                    	data.status = rows2[0].status;
		                    	data.sender_id = rows2[0].sender_id;
		                    	data.sender_type = rows2[0].sender_type
		                    }else{
		                    	data.status = 'Add';
		                    	data.sender_id = ''
		                    	data.sender_type = ''
		                    }

		                    final_data[i]  = data;
		                    i++;
		                    return callback(null, final_data);
		                });
		            },function(err,results){
		                res.send({
		                    "status":1,
		                    "message":"Successful",
		                    "data" : final_data
		                });
		            });
        		}
        	})
		}
    });
}

module.exports.request_client = function(req,res){
	con.query("SELECT id from client_trainer_relationship where trainer_id = '" + req.body.trainer_id + "' and user_id = '" + req.body.user_id + "' ",function(err,data_rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while fetching relationship data"
	        })
		}else{
			if(data_rows.length){
				res.send({
		            "status":0,
		            "message":"Waiting for confirmation"
		        })
			}else{
				con.query("SELECT name from users where id = '" + req.body.user_id + "' ",function(err,client_rows){
					if(err){
						res.send({
				            "status":0,
				            "message":"There is an error while fetching client data"
				        })
					}else{
						if(!client_rows.length){
							res.send({
					            "status":0,
					            "message":"Client doesnot exist"
					        })
						}else{
							var created_at = Math.floor(new Date().valueOf() / 1000);
							con.query("INSERT INTO client_trainer_relationship (user_id, trainer_id, status, sender_id, sender_type, created_at) VALUES ('" + req.body.user_id + "', '" + req.body.trainer_id + "', '" + req.body.status + "', '" + req.body.trainer_id + "', 'trainer', '" + created_at + "')",function(err,user_result){
								if(err){
									res.send({
							            "status":0,
							            "message":"There is an error while inserting client-trainer relationship data"
							        })
								}else{
									con.query("SELECT device_token from users where id = '" + req.body.user_id + "' and notification = 'yes' ",function(err,client_data){
										if(err){
											res.send({
								                "status":0,
								                "message":"There is an error while fetching device token of the client"
								            })
										}else{
											con.query("SELECT name from trainers where id = '" + req.body.trainer_id + "' ",function(err,trainer_data){
												if(err){
													res.send({
										                "status":0,
										                "message":"There is an error while fetching name of the client"
										            })
												}else{
													var message = {
										    			to: client_data[0].device_token,
													    notification: {
													        title: 'Request', 
													        body: trainer_data[0].name + ' wants to be your friend'
													    },
													    data: {
										                    "notification_type" : 'Request'
										                }
													};
													fcm.send(message,function(err,response){
													    if(err)
													       	console.log('Error')  
													    else
													    	console.log('No Error')     
													})
													res.send({
										                "status":1,
										                "message":"A request has been sent to the client"
										            })
												}
											})
										}
									})
								}
							})
						}
					}
				})
			}
		}
	})
}

module.exports.get_client_profile = function(req,res){
	con.query("SELECT * from users where id = '" + req.body.client_id + "' ",function(err,rows){
		if(err){
			res.send({
                "status":0,
                "message":"There is an error while fetching client profile"
            })
		}else{
			var room_name = req.body.trainer_id + '_' + req.body.client_id;
			con.query("SELECT room_id from rooms where room_name = '" + room_name + "' ",function(err,rows2){
				if(err){
					res.send({
		                "status":0,
		                "message":"There is an error while fetching room data"
		            })
				}else{
					if(rows2.length)
		                rows[0].room_id = rows2[0].room_id
		            else
		                rows[0].room_id = '';

					res.send({
			            "status":1,
			            "message":"Client profile data",
			            "data":rows[0]
			        })
				}
			})
		}
	})
}

module.exports.get_group_request_status = function(req,res){
	con.query("SELECT is_deleted from groups where id = '" + req.body.group_id + "' ",function(err,check_rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while adding group members"
	        })
		}else{
			if(check_rows[0].is_deleted == 'yes'){
				con.query("DELETE from group_requests where group_id = '" + req.body.group_id + "' and client_id = '" + req.body.client_id + "' ",function(err,result3){
					if(err){
						res.send({
				            "status":0,
				            "message":"There is an error while deleting group request"
				        })
					}else{
						res.send({
				            "status":0,
				            "message":"This group is no longer available"
				        })
					}
				})
			}else{
				if(req.body.status == 'Accepted'){
					con.query("SELECT id from group_members where group_id = '" + req.body.group_id + "' and member_id = '" + req.body.client_id + "' ",function(err,check_data){
						if(err){
							res.send({
					            "status":0,
					            "message":"There is an error while adding group members"
					        })
						}else{
							if(!check_data.length){
								con.query("INSERT INTO group_members (group_id, member_id, is_removed) VALUES ('" + req.body.group_id + "', '" + req.body.client_id + "', '0')",function(err,result2){
									if(err){
										res.send({
								            "status":0,
								            "message":"There is an error while adding group members"
								        })
									}
								});
							}else{
								con.query("UPDATE group_members SET is_removed = '0' where group_id = '" +  req.body.group_id + "' and member_id = '" + req.body.client_id + "' ",function(err,result3){
									if(err)
										throw err;
								})
							}
						}
					})
				}
				con.query("SELECT groups.name as group_name,trainers.device_token,users.name from group_requests inner join trainers on group_requests.trainer_id = trainers.id inner join users on group_requests.client_id = users.id inner join groups on group_requests.group_id = groups.id where group_requests.group_id = '" + req.body.group_id + "' and group_requests.client_id = '" + req.body.client_id + "' ",function(err,users_data){
					if(err){
						res.send({
				            "status":0,
				            "message":"There is an error while sending notification to the trainer"
				        })
					}else{
						con.query("DELETE from group_requests where group_id = '" + req.body.group_id + "' and client_id = '" + req.body.client_id + "' ",function(err,result){
							if(err){
								res.send({
						            "status":0,
						            "message":"There is an error while deleting group request"
						        })
							}else{
								var message = {
					    			to: users_data[0].device_token,
								    notification: {
								        title: 'Request ' + req.body.status, 
								        body: users_data[0].name + ' ' + req.body.status + ' your ' + users_data[0].group_name + ' join request'
								    },
								    data: {
					                    "notification_type" : 'group_request_response'
					                }
								};
								fcm.send(message,function(err,response){
								    if(err)
								       	console.log('Error')  
								    else
								    	console.log('No Error')     
								})
							}
						})
					}
				})
				res.send({
			        "status":1,
			        "message":"Request " + req.body.status
			    })
			}
		}
	})
}

