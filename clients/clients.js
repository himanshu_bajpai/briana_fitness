var con = require('../connection/connection');
var FCM = require('fcm-node');
var serverKey = 'AAAADJ64yO4:APA91bEd6FHBDYaORNh4FeUh81PSa8U_oQBTAaxV8A8r8Kv2v-HXaYcqOc8R_r2kAVgv1QkM5ZsPzhwj9rhohOgf3CeHbCed_TL9FzwwxM6b5ZeZdSpgXiHR25xbY2cgrX_DhnfLD9Na';
var fcm = new FCM(serverKey);
var async = require('async');

module.exports.get_all_trainers = function(req,res){
	con.query("Select GROUP_CONCAT(trainer_id) as trainer_id from client_trainer_relationship where user_id = '" + req.body.user_id + "' and status = 'Accepted' ",function(err,rows){
        if(err){
			res.send({
                "status":0,
                "message":"There is an error while fetching client_trainer_relationship data"
            })
		}else{
			if(rows[0].trainer_id == null)
				var query = "SELECT id, name, email, profile_image from trainers";
			else
				var query = "SELECT id, name, email, profile_image from trainers where id NOT IN (" + rows[0].trainer_id + ") ";

			con.query(query,function(err, rows2){
        		if(err){
        			res.send({
		                "status":0,
		                "message":"There is an error while fetching trainer data"
		            })
        		}else{
        			var final_data = [];
            		var i = 0;
		            async.mapSeries(rows2, function(data,callback){
		                con.query("SELECT status, sender_id, sender_type from client_trainer_relationship where user_id = '" + req.body.user_id + "' and trainer_id = '" + data.id + "' ", function (err,rows2){
		                    if(err){
		                        res.send({
		                            "status":0,
		                            "message":"error ocurred while fetching data"
		                        })
		                    }
		                    if(rows2 != ''){
		                    	data.status = rows2[0].status;
		                    	data.sender_id = rows2[0].sender_id;
		                    	data.sender_type = rows2[0].sender_type
		                    }else{
		                    	data.status = 'Add';
		                    	data.sender_id = ''
		                    	data.sender_type = ''
		                    }

		                    final_data[i]  = data;
		                    i++;
		                    return callback(null, final_data);
		                });
		            },function(err,results){
		                res.send({
		                    "status":1,
		                    "message":"Successful",
		                    "data" : final_data
		                });
		            });
        		}
        	})
		}
    });
}

module.exports.request_trainer = function(req,res){
	con.query("SELECT id from client_trainer_relationship where trainer_id = '" + req.body.trainer_id + "' and user_id = '" + req.body.user_id + "' ",function(err,data_rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while fetching relationship data"
	        })
		}else{
			if(data_rows.length){
				res.send({
		            "status":0,
		            "message":"Waiting for confirmation"
		        })
			}else{
				con.query("SELECT name from trainers where id = '" + req.body.trainer_id + "' ",function(err,trainer_rows){
					if(err){
						res.send({
				            "status":0,
				            "message":"There is an error while fetching trainer data"
				        })
					}else{
						if(!trainer_rows.length){
							res.send({
					            "status":0,
					            "message":"Trainer doesnot exist"
					        })
						}else{
							var created_at = Math.floor(new Date().valueOf() / 1000);
							con.query("SELECT id from client_trainer_relationship where user_id = '" + req.body.user_id + "' and trainer_id = '" + req.body.trainer_id + "' ",function(err,check_relationship){
								if(err){
									res.send({
							            "status":0,
							            "message":"There is an error while checking relationship data"
							        })
								}else{
									if(!check_relationship.length){
										con.query("INSERT INTO client_trainer_relationship (user_id, trainer_id, status, sender_id, sender_type, created_at) VALUES ('" + req.body.user_id + "', '" + req.body.trainer_id + "', '" + req.body.status + "', '" + req.body.user_id + "', 'client', '" + created_at + "')",function(err,user_result){
											if(err){
												res.send({
										            "status":0,
										            "message":"There is an error while inserting client-trainer relationship data"
										        })
											}else{
												con.query("SELECT device_token from trainers where id = '" + req.body.trainer_id + "' and notification = 'yes' ",function(err,trainer_data){
													if(err){
														res.send({
											                "status":0,
											                "message":"There is an error while fetching device token of the trainer"
											            })
													}else{
														con.query("SELECT name from users where id = '" + req.body.user_id + "' ",function(err,client_data){
															if(err){
																res.send({
													                "status":0,
													                "message":"There is an error while fetching name of the client"
													            })
															}else{
																var message = {
													    			to: trainer_data[0].device_token,
																    notification: {
																        title: 'Request', 
																        body: client_data[0].name + ' wants to be your friend'
																    },
																    data: {
													                    "notification_type" : 'Request'
													                }
																};
																fcm.send(message,function(err,response){
																    if(err)
																       	console.log(err)
																    else
																    	console.log('elseeeeeeee')     
																})
																res.send({
													                "status":1,
													                "message":"A request has been sent to the trainer"
													            })
															}
														})
													}
												})
											}
										})
									}else{
										res.send({
							                "status":0,
							                "message":"Already Requested"
							            })
									}
								}
							})
						}
					}
				})
			}
		}
	});
}

module.exports.get_trainer_list = function(req,res){
	con.query("Select client_trainer_relationship.trainer_id as id, trainers.name, trainers.email, trainers.city_state, trainers.profile_image from client_trainer_relationship INNER JOIN trainers ON client_trainer_relationship.trainer_id = trainers.id where client_trainer_relationship.user_id = '" + req.body.user_id + "' and client_trainer_relationship.status = 'Accepted' ",function(err,rows){
		if(err){
			res.send({
                "status":0,
                "message":"There is an error while fetching client data"
            })
		}else{
			if(!rows.length){
				res.send({
		            "status":1,
		            "message":"Trainer doesnot exist"
		        })
			}else{
				var final_data = [];
            	var i = 0;
	            async.mapSeries(rows, function(data,callback){ 
					var room_name = data.id + '_' + req.body.user_id;
	            	con.query("SELECT room_id from rooms where room_name = '" + room_name + "' ",function(err,rows2){
						if(err)
							throw err;

						if(rows2 != '')
		                    data.room_id = rows2[0].room_id
		                else
		                   	data.room_id = '';

						return callback(null, data);
					})
	            },function(err,results){
	                res.send({
			            "status":1,
			            "message":"Trainer exist",
			            "data" : results
			        })
	            });
			}
		}
	})
}

module.exports.get_requested_trainer_list = function(req,res){
	con.query("Select client_trainer_relationship.trainer_id as id, trainers.name, trainers.profile_image,trainers.city_state from client_trainer_relationship INNER JOIN trainers ON client_trainer_relationship.trainer_id = trainers.id where client_trainer_relationship.user_id = '" + req.body.user_id + "' and client_trainer_relationship.status = 'Requested' and client_trainer_relationship.sender_type != 'client' ",function(err,data){
		if(err){
			res.send({
                "status":0,
                "message":"There is an error while fetching trainer data"
            })
		}else{
			if(!data.length){
				res.send({
		            "status":0,
		            "message":"Trainer doesnot exist"
		        })
			}else{
				res.send({
		            "status":1,
		            "message":"Trainer exist",
		            "data" : data
		        })
			}
		}
	})
}

module.exports.request_status_trainer = function(req,res){
	con.query("SELECT name from trainers where id = '" + req.body.trainer_id + "' ",function(err,trainer_rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while fetching trainer data"
	        })
		}else{
			if(!trainer_rows.length){
				res.send({
		            "status":0,
		            "message":"Trainer doesnot exist"
		        })
			}else{
				con.query("UPDATE client_trainer_relationship SET status = '" + req.body.status + "' where user_id = '" + req.body.user_id + "' and trainer_id = '" + req.body.trainer_id + "' ",function(err, result){
					if(err){
						res.send({
			                "status":0,
			                "message":"There is an error while updating client-trainer relationship data"
			            })
					}else{
						con.query("SELECT name from users where id = '" + req.body.user_id + "' ",function(err,client_data){
							if(err){
								res.send({
					                "status":0,
					                "message":"There is an error while fetching client data"
					            })
							}else{
								con.query("SELECT device_token from trainers where id = '" + req.body.trainer_id + "' and notification = 'yes' ",function(err,trainer_data){
									if(err){
										res.send({
							                "status":0,
							                "message":"There is an error while fetching trainer data"
							            })
									}else{
										if(req.body.status == 'Accepted'){
											var title = 'Request Accepted';
											var body = 'Your request has been accepted by ' + client_data[0].name;
											var status = 1;
										}else{
											var title = 'Request Rejected';
											var body = 'Your request has been rejected by ' + client_data[0].name;
											var status = 1;
											con.query("DELETE from client_trainer_relationship where status = 'Rejected' ", function(err, result2){
												if(err){
													res.send({
										                "status":0,
										                "message":"There is an error while rejecting request"
										            })
												}
											})
										}
										if(trainer_data != ''){
											var message = {
								    			to: trainer_data[0].device_token,
											    notification: {
											        title: title, 
											        body: body
											    },
											    data:{
											    	"notification_type" : "request_response"
											    }
											};
											fcm.send(message,function(err,response){
											    if(err)
											       	console.log(err);
											    else
											    	console.log('elseeeeeeee')     
											})
										}
										
										res.send({
							                "status":status,
							                "message":body
							            })
									}
								})
							}
						})
					}
				})
			}
		}
	})
}

module.exports.get_fitness_levels = function(req,res){
	con.query("SELECT * from fitness_levels",function(err,rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while fetching fitness levels"
	        })
		}else{
			res.send({
	            "status":1,
	            "message":"Fitness levels",
	            "data":rows
	        })
		}
	})
}

module.exports.get_requested_group_requests = function(req,res){
	con.query("SELECT trainers.name as trainer_name, trainers.profile_image as trainer_image,groups.id as group_id, groups.name as group_name from group_requests inner join trainers on group_requests.trainer_id = trainers.id inner join groups on group_requests.group_id = groups.id where group_requests.client_id = '" + req.body.client_id + "' and group_requests.status = 'Requested' ",function(err,rows){
		if(err){
			res.send({
	            "status":0,
	            "message":"There is an error while fetching group request data"
	        })
		}else{
			if(!rows.length){
				res.send({
					"status":0,
					"message":"No group request"
				})
			}else{
				res.send({
		            "status":1,
		            "message":"Group request",
		            "data":rows
		        })
			}
		}
	})
}

